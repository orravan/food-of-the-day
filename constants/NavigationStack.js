import {
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator
} from "react-navigation";
import Splashscreen from "../screens/Splashscreen";
import CredentialsScreen from "../screens/CredentialsScreen";
import ForgetPasswordScreen from "../screens/ForgetPasswordScreen";
import LoginScreen from "../screens/LoginScreen";
import MenuScreen from "../screens/MenuScreen";
import CartScreen from "../screens/CartScreen";
import DeliveryScreen from "../screens/DeliveryScreen";
import ValidateOrderScreen from "../screens/ValidateOrderScreen";
import CreditCardScreen from "../screens/CreditCardScreen";
import PaymentChoiceScreen from "../screens/PaymentChoiceScreen";
import AllergyScreen from "../screens/AllergyScreen";
import MyAccountScreen from "../screens/MyAccountScreen";
import MyOrdersScreen from "../screens/MyOrdersScreen";
import LogoutScreen from "../screens/LogoutScreen";
import AcknowledgmentScreen from "../screens/AcknowledgmentScreen";

const SplashStack = createStackNavigator({
  Splash: {
    screen: Splashscreen,
    navigationOptions: {
      header: null
    }
  }
});

const MenuStack = createStackNavigator({
  Menu: {
    screen: MenuScreen,
    navigationOptions: {
      title: "Menu"
    }
  }
});

const MyAccountStack = createStackNavigator({
  MyAccount: {
    screen: MyAccountScreen,
    navigationOptions: {
      title: "Mon compte"
    }
  }
});

const MyOrderStack = createStackNavigator({
  MyOrders: {
    screen: MyOrdersScreen,
    navigationOptions: {
      title: "Mes commandes"
    }
  }
});

const LogoutStack = createStackNavigator({
  Logout: {
    screen: LogoutScreen
  }
});

const DrawerStack = createDrawerNavigator(
  {
    Menu: {
      screen: MenuStack,
      navigationOptions: {
        title: "Menu"
      }
    },
    MyAccount: {
      screen: MyAccountStack,
      navigationOptions: {
        title: "Mon compte"
      }
    },
    MyOrders: {
      screen: MyOrderStack,
      navigationOptions: {
        title: "Mes commandes"
      }
    },
    Logout: {
      screen: LogoutStack,
      navigationOptions: {
        title: "Se déconnecter"
      }
    }
  },
  {
    contentOptions: {
      activeTintColor: "#ff6600"
    }
  }
);

const MainStack = createStackNavigator({
  Drawer: {
    screen: DrawerStack,
    navigationOptions: {
      header: null
    }
  },
  Delivery: {
    screen: DeliveryScreen,
    navigationOptions: {
      title: "Livraison",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  ValidateOrder: {
    screen: ValidateOrderScreen,
    navigationOptions: {
      title: "Récapitulatif",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  CreditCard: {
    screen: CreditCardScreen,
    navigationOptions: {
      title: "Carte de paiement",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  PaymentChoice: {
    screen: PaymentChoiceScreen,
    navigationOptions: {
      title: "Modes de paiement",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  Allergy: {
    screen: AllergyScreen,
    navigationOptions: {
      title: "Allergies",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  Acknowledgment: {
    screen: AcknowledgmentScreen,
    navigationOptions: {
      title: "Commande validée",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      title: "Se connecter",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  Credentials: {
    screen: CredentialsScreen,
    navigationOptions: {
      title: "S'inscrire",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  },
  ForgetPassword: {
    screen: ForgetPasswordScreen,
    navigationOptions: {
      title: "Mot de passe",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      }
    }
  }
});

const AppStack = createStackNavigator(
  {
    Main: {
      screen: MainStack,
      navigationOptions: {
        header: null
      }
    },
    Cart: {
      screen: CartScreen,
      navigationOptions: {
        title: "Panier",
        headerStyle: {
          backgroundColor: "#ff6600"
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          flex: 1,
          textAlign: "center",
          alignSelf: "center",
          justifyContent: "center"
        }
      }
    }
  },
  {
    mode: "modal"
  }
);

export default createSwitchNavigator({
  Splash: SplashStack,
  App: AppStack
});
