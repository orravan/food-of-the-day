![Food of the day](https://i.screenshot.net/po2gvul)

# Food Of The Day

Food Of the Day (FOD) is a native application developed in React Native for a French company in Montpellier. It's a e-shop food application who gives the possibility to see meals purposed by the company and order it. The customer pays directly in the app, chooses a delivery point (e.g. a company or a gym based on Montpellier), he can see his order status and receive a notification (+ email) when the order is paid and delivered.

## Getting started

FOD uses tools below to work :

![Contentful](https://i.screenshot.net/wp4jqh6)
![Stripe](https://i.screenshot.net/3k86mbn)
![OneSignal](https://i.screenshot.net/ndxw7sk)
![Rollbar](https://i.screenshot.net/mx4dqco)
![Mailjet](https://i.screenshot.net/7yo3dcw)
![Serverless](https://i.screenshot.net/7yozqcz)

- [Contentful](https://www.contentful.com/) : CMS used in this project to manage meals, delivery points and meal types availables on application. This is a extract of Content model used in Contentful : [contentful-export.json](https://srv-file5.gofile.io/download/Ivo3oS/contentful-export.json)
- [Stripe](https://stripe.com/) : Online payment solution to manage customers informations, orders and payments.
- [OneSignal](https://onesignal.com/) : Push notifications tools used to send native notification when an order is delivered to a delivery point or if an order is refund for example.
- [Rollbar](https://rollbar.com/) : Error tracking and debugging tools to centralize errors on application.
- [Mailjet](https://fr.mailjet.com/) : Emailing solution to send transactional or campaign emails. In this project we use it mainly to send email in theses use cases : account creation, reset password, order confirmation, order delivery or order refund.
- [Serverless](https://serverless.com/) API project : [Food of the day servelerss](https://gitlab.com/orravan/food-of-the-day-serverless) securised Node JS API (JWT) which centralizes calls between Stripe, OneSignal and Mailjet.

## Initial Configuration

1. Create an account on : Contentful, Stripe, OneSignal, Rollbar, Mailjet and AWS.

2. Go to [Food of the day servelerss](https://gitlab.com/orravan/food-of-the-day-serverless) and follow instruction of"Initial configuration" paragraph.

3. Copy .env.dist to .env and fill it with informations requested.

4. Install CLI tools : `npm install -g react-native expo`. If you want to test on Android simulator, it requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup). iOS simulator is available by default on Mac OS.

## Available Scripts

### `npm install`

Install all dependencies (e.g. redux sauce, lodash ...etc)

### `npm start`

or

### `expo start`

Runs your app in development mode.

Open it in the [Expo app](https://expo.io) on your phone to view it. It will reload if you save edits to your files, and you will see build errors and logs in the terminal.

Sometimes you may need to reset or clear the React Native packager's cache. To do so, you can pass the `--reset-cache` flag to the start script:

```
npm start --reset-cache
```

#### `npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### `npm run ios`

Like `npm start`, but also attempts to open your app in the iOS Simulator if you're on a Mac and have it installed.

#### `npm run android`

Like `npm start`, but also attempts to open your app on a connected Android device or emulator. Requires an installation of Android build tools (see [React Native docs](https://facebook.github.io/react-native/docs/getting-started.html) for detailed setup).

#### `npm run eject`

This will start the process of "ejecting" from Create React Native App's build scripts. You'll be asked a couple of questions about how you'd like to build your project.

**Warning:** Running FOD app eject is a permanent action (aside from whatever version control system you use). An ejected app will require you to have an [Xcode and/or Android Studio environment](https://facebook.github.io/react-native/docs/getting-started.html) set up.

## Workflow

This a typical customer workflow on FOD app :

1. Home screen : you can see meals proposed on different delivery dates

![Home](https://i.screenshot.net/l8jxa08)

2. Card meal : you can choose extra items and add to cart the meal

![card](https://i.screenshot.net/xgj7ug4)

3. Check your cart and click on "Next"

![cart](https://i.screenshot.net/ewx0h16)

4. If you've already an account login, else create and account (you'll receive an confirmation email). You can reset your password if you've forgotten it.

![login](https://i.screenshot.net/opvqh36)
![signup](https://i.screenshot.net/r52etxx)
![reset](https://i.screenshot.net/726rak0)

5. Choose a delivery point

![delivery](https://i.screenshot.net/qjlptle)

6. If you don't have ever registered your credit card number, provide it or choose an existing one.

![credit](https://i.screenshot.net/n53yt91)

7. Summary of the order before validate it

![summary](https://i.screenshot.net/9jn5t8n)

8. Order confirmation (you'll receive a confirmation email)

![confirmation](https://i.screenshot.net/lqnghk9)

9. My orders : check order status and receive a notification on phone + an email when the order is develired (Stripe order status changed to "Finished")

## Application available on stores

Available on [App Store](https://apps.apple.com/us/app/fod/id1460729176?l=fr&ls=1) and [Google Play Store](https://play.google.com/store/apps/details?id=com.foodoftheday)

## Contributing

Interested in contributing to this repo? Just follow [Git flow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) and submit a PR for a new feature/hotfix.

## Licensing

The code in this project is licensed under MIT license.
