import {
  CONTENTFUL_API_BASE_URL,
  CONTENTFUL_API_SPACE_ID,
  CONTENTFUL_API_TOKEN
} from "react-native-dotenv";
import _ from "lodash";
import moment from "moment";

/**
 * Get Meals from Contentful
 */
const getMeals = async () => {
  try {
    let response = await fetch(
      `${CONTENTFUL_API_BASE_URL}/spaces/${CONTENTFUL_API_SPACE_ID}/entries?access_token=${CONTENTFUL_API_TOKEN}&content_type=menu`
    );
    let { items, includes } = await response.json();
    // We link the photo URL to each item from the Assets array
    let meals = items.map(i => {
      const itemPicture = i.fields.photos
        ? _.find(includes.Asset, a => a.sys.id === i.fields.photos[0].sys.id)
        : null;
      i.picture = itemPicture ? itemPicture.fields.file.url : "";
      return i;
    });

    // Get meal type name
    meals = items.map(i => {
      const typeMeal = _.find(
        includes.Entry,
        a => a.sys.id === i.fields.typeMeal.sys.id
      );
      i.typeMealName = typeMeal ? typeMeal.fields.name : "";
      i.typeMealIcon = typeMeal ? typeMeal.fields.icon : "";
      return i;
    });

    // Get meal's extra if there's one
    meals = items.map(i => {
      if (!i.fields.extra) {
        return i;
      }

      if (!i.extra) {
        i.extra = [];
      }
      i.fields.extra.map((e, index) => {
        const extra = _.find(includes.Entry, a => a.sys.id === e.sys.id);

        if (extra) {
          extra.fields.selected = false;
          extra.fields.id = e.sys.id;

          i.extra[index] = extra.fields;
        }
      });

      return i;
    });

    // Display only meals befoce limit date chekckout
    meals = meals.filter(key => {
      if (moment().isBefore(key.fields.limitDateCheckout)) {
        return key;
      }
    });

    // We group by shipping date
    let mealsByShippingDate = _.groupBy(meals, function(o) {
      return moment(o.fields.shippingDate)
        .startOf("day")
        .format();
    });

    meals = _.chain(mealsByShippingDate)
      .map(function(meals, shippingDate) {
        return {
          shippingDate: shippingDate,
          limitDateCheckout: meals[0].fields.limitDateCheckout,
          meals: meals
        };
      })
      .sortBy("shippingDate")
      .value();

    return meals;
  } catch (error) {
    console.error(error);
    return [];
  }
};

/**
 * Get delivery points from Contenful
 */
const getDeliveryPoints = async () => {
  try {
    let response = await fetch(
      `${CONTENTFUL_API_BASE_URL}/spaces/${CONTENTFUL_API_SPACE_ID}/entries?access_token=${CONTENTFUL_API_TOKEN}&content_type=deliveryPoint`
    );
    let { items, includes } = await response.json();
    // We link the photo URL to each item from the Assets array
    let deliveryPoints = items.map(i => {
      if (i.fields.backgroundImage) {
        const itemPicture = _.find(
          includes.Asset,
          a => a.sys.id === i.fields.backgroundImage.sys.id
        );
        i.picture = itemPicture ? itemPicture.fields.file.url : "";
      } else {
        i.picture = "";
      }

      return i;
    });

    return deliveryPoints;
  } catch (error) {
    console.error(error);
    return [];
  }
};

export default {
  getMeals,
  getDeliveryPoints
};
