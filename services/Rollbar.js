import { Client } from "rollbar-react-native";
import { ROLLBAR_API_TOKEN } from "react-native-dotenv";

exports.rollbar = new Client(`${ROLLBAR_API_TOKEN}`);
