import { FOD_SLS_API_BASE_URL } from "react-native-dotenv";
import OneSignal from "./OneSignal";
import _ from "lodash";
import moment from "moment/min/moment-with-locales";

/**
 * Login user to Stripe
 */
const login = async user => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/user/login`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: user.email,
        password: user.password
      })
    });

    if (response.ok) {
      let { token } = await response.json();

      let userInformations = await fetch(`${FOD_SLS_API_BASE_URL}/user/me`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: token
        }
      });

      if (userInformations.ok) {
        let userLoggedIn = await userInformations.json();

        // We attach user to One Signal notifications
        OneSignal.logOSUser(userLoggedIn);

        return {
          firstname: userLoggedIn.metadata.prenom,
          lastname: userLoggedIn.metadata.nom,
          email: userLoggedIn.email,
          phone: userLoggedIn.metadata.numero,
          password: userLoggedIn.metadata.motDePasse,
          cards:
            userLoggedIn.sources && userLoggedIn.sources.data.length > 0
              ? userLoggedIn.sources.data
              : [],
          allergy:
            userLoggedIn.metadata.allergy &&
            userLoggedIn.metadata.allergy.length > 0
              ? userLoggedIn.metadata.allergy
              : "",
          token: token,
          stripeId: null
        };
      } else {
        throw Error(userInformations.status);
      }
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Register user on Stripe
 */
const register = async user => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/user/register`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        lastname: user.lastname,
        firstname: user.firstname,
        phonenumber: user.phone,
        email: user.email,
        allergy: "",
        password: user.password
      })
    });

    if (response.ok) {
      let { token } = await response.json();

      let userInformations = await fetch(`${FOD_SLS_API_BASE_URL}/user/me`, {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization: token
        }
      });

      if (userInformations.ok) {
        let userLoggedIn = await userInformations.json();

        // We attach user to One Signal notifications
        OneSignal.logOSUser(userLoggedIn);

        return {
          firstname: userLoggedIn.metadata.prenom,
          lastname: userLoggedIn.metadata.nom,
          email: userLoggedIn.email,
          phone: userLoggedIn.metadata.numero,
          password: userLoggedIn.metadata.motDePasse,
          cards:
            userLoggedIn.sources && userLoggedIn.sources.data.length > 0
              ? userLoggedIn.sources.data
              : [],
          allergy:
            userLoggedIn.metadata.allergy &&
            userLoggedIn.metadata.allergy.length > 0
              ? userLoggedIn.metadata.allergy
              : "",
          token: token,
          stripeId: null
        };
      } else {
        throw Error(userInformations.status);
      }
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Update user on Stripe
 */
const update = async user => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/user/update`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: user.token
      },
      body: JSON.stringify({
        lastname: user.lastname,
        firstname: user.firstname,
        phonenumber: user.phone,
        email: user.email,
        allergy: user.allergy,
        password: user.password
      })
    });

    if (response.ok) {
      let { token } = await response.json();

      return {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        phone: user.phone,
        password: "",
        cards: user.cards,
        allergy: user.allergy,
        token: token
      };
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Delete a credit card for user on Stripe
 */
const deleteCard = async (user, cardId) => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/card/delete`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      },
      body: JSON.stringify({
        cardId: cardId
      })
    });

    if (response.ok) {
      _.remove(user.cards, function(e) {
        return e.id === cardId;
      });

      return {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        phone: user.phone,
        password: "",
        allergy: user.allergy,
        cards: user.cards,
        token: user.token
      };
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Add a credit card to user
 * @param {*} user
 */
const addCard = async (number, expMonth, expYear, cvc, name, user) => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/card/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      },
      body: JSON.stringify({
        name: name,
        number: number,
        expMonth: expMonth,
        expYear: expYear,
        cvc: cvc,
        cardId: null
      })
    });

    if (response.ok) {
      let card = await response.json();
      user.cards.push(card);

      return {
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        phone: user.phone,
        password: user.password,
        allergy: user.allergy,
        cards: user.cards,
        token: user.token,
        stripeId: null
      };
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Pay order
 * @param {*} user
 */
const payOrder = async (
  deliveryDate,
  deliveryPlace,
  items,
  user,
  coupon,
  creditCardUsed
) => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/order/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      },
      body: JSON.stringify({
        sourceId: creditCardUsed,
        metadata: {
          dateLivraison: deliveryDate,
          lieuLivraison: deliveryPlace,
          alergies:
            user.allergy && user.allergy !== "" ? user.allergy : "Aucune"
        },
        items: items,
        coupon: coupon !== "" ? coupon : false
      })
    });

    if (response.ok) {
      let order = await response.json();

      return order.id;
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Check if coupon is valid
 * @param {*} coupon
 */
const checkCoupon = async (coupon, user) => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/coupon`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: user.token
      },
      body: JSON.stringify({
        coupon: coupon.split(" ").join("_")
      })
    });

    if (response.ok) {
      let couponResponse = await response.json();

      return couponResponse;
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * List user's orders
 * @param {*} user
 */
const listOrders = async user => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/order/list`, {
      method: "GET",
      headers: {
        Authorization: user.token
      }
    });

    if (response.ok) {
      let orders = await response.json();

      let ordersByShippingDate = _.groupBy(orders.data, function(order) {
        return order.metadata.dateLivraison;
      });

      listOrders = _.chain(ordersByShippingDate)
        .map(function(orders, shippingDate) {
          return {
            shippingDate: shippingDate,
            display: true,
            expanded: false,
            orders: orders
          };
        })
        .sortBy("shippingDate")
        .value();

      return _.sortBy(listOrders, function(order) {
        return new Date(moment(order.shippingDate, "ddd D MMM YYYY").format());
      }).reverse();
    } else {
      console.log(response);
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

/**
 * Reset password for user
 */
const resetPassword = async user => {
  try {
    let response = await fetch(`${FOD_SLS_API_BASE_URL}/user/reset`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: user.email
      })
    });

    if (response.ok) {
      let message = await response.json();

      return message;
    } else {
      throw Error(response.status);
    }
  } catch (error) {
    console.log(error);
    throw error;
  }
};

export default {
  login,
  register,
  update,
  deleteCard,
  addCard,
  payOrder,
  checkCoupon,
  listOrders,
  resetPassword
};
