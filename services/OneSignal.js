import OneSignal from "react-native-onesignal";
import { ONE_SIGNAL_APP_ID } from "react-native-dotenv";
import { rollbar } from "./Rollbar";

const initOneSignal = () => {
  OneSignal.init(ONE_SIGNAL_APP_ID, {
    kOSSettingsKeyInFocusDisplayOption: 2,
    kOSSettingsKeyAutoPrompt: false
  });
  OneSignal.addEventListener("received", notification => {
    console.log("Notification received: ", notification);
  });
  OneSignal.addEventListener("opened", openResult => {
    console.log("Message: ", openResult.notification.payload.body);
    console.log("Data: ", openResult.notification.payload.additionalData);
    console.log("isActive: ", openResult.notification.isAppInFocus);
    console.log("openResult: ", openResult);
  });
  OneSignal.addEventListener("ids", device => {
    console.log("Device info: ", device);
  });
};

const removeNotificationsListeners = () => {
  OneSignal.removeEventListener("received");
  OneSignal.removeEventListener("opened");
  OneSignal.removeEventListener("ids");
};

const logOSUser = user => {
  rollbar.setPerson(
    user.id,
    user.metadata.prenom + " " + user.metadata.nom,
    user.email
  );
  OneSignal.setExternalUserId(user.id);
};

const logOut = () => {
  OneSignal.removeExternalUserId();
  rollbar.clearPerson();
};

const checkPermissions = () => {
  OneSignal.checkPermissions(permissions => {
    console.log(permissions);
    if (!permissions.badge) {
      OneSignal.registerForPushNotifications();
    }
  });
};

const toggleNotificationsPermissions = () => {
  OneSignal.getPermissionSubscriptionState(status => {
    OneSignal.setSubscription(!status);
  });
};

export default {
  initOneSignal,
  removeNotificationsListeners,
  logOSUser,
  logOut,
  toggleNotificationsPermissions,
  checkPermissions
};
