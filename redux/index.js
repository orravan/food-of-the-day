import { combineReducers } from 'redux'
import { createStore } from 'redux'


export default () => {
  /* ------------- Assemble The Reducers ------------- */
  const reducer = combineReducers({
    root: require('./RootRedux').reducer,
  })

  return createStore(reducer)
}
