import { createReducer, createActions } from "reduxsauce";
import _ from "lodash";
import { removeIn } from "immutable";

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  setUser: ["user"],
  setMeals: ["meals"],
  setDeliveryPoints: ["deliveryPoints"],
  addItem: ["items"],
  deleteItem: ["item"],
  deleteExtra: ["extra"],
  setPlace: ["place"],
  setCoupon: ["coupon"],
  setCouponId: ["couponId"],
  setCouponType: ["couponType"],
  setCreditCardUsed: ["creditCardUsed"],
  expandCard: ["index"],
  clearCart: [],
  clearData: []
});

export const RootTypes = Types;
export default Creators;

/* ------------- Initial State ------------- */

export const INITIAL_STATE = {
  user: {
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    password: "",
    allergy: "",
    cards: [],
    token: "",
    stripeId: null
  },
  meals: [],
  deliveryPoints: [],
  place: null,
  cart: [],
  creditCardUsed: null,
  expandedCard: 0,
  coupon: "",
  couponId: null,
  couponType: null
};

/* ------------- Reducers ------------- */

export const setUser = (state, { user }) => ({ ...state, user });

export const setMeals = (state, { meals }) => ({ ...state, meals });

export const setDeliveryPoints = (state, { deliveryPoints }) => ({
  ...state,
  deliveryPoints
});

export const addItem = (state, { items }) => ({
  ...state,
  expandedCard: 0,
  cart: _.flatten([...state.cart, items])
});

export const deleteItem = (state, { item }) => ({
  ...state,
  cart: state.cart.filter(i => i.sys.id !== item.id)
});

export const deleteExtra = (state, { extra }) => ({
  ...state,
  cart: state.cart.map(i => ({
    ...i,
    extra: _.filter(i.extra, e => e.id !== extra.id)
  }))
});

export const setPlace = (state, { place }) => ({ ...state, place: place });

export const expandCard = (state, { index }) => ({
  ...state,
  expandedCard: index
});

export const clearCart = state => ({
  ...state,
  cart: [],
  coupon: "",
  couponId: null,
  couponType: null,
  expandedCard: 0
});

export const clearData = state => ({
  ...state,
  user: {
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    password: "",
    allergy: "",
    cards: [],
    token: "",
    stripeId: null
  },
  place: null,
  cart: [],
  creditCardUsed: null,
  expandedCard: 0,
  coupon: "",
  couponId: null,
  couponType: null
});

export const setCoupon = (state, { coupon }) => ({ ...state, coupon: coupon });

export const setCouponId = (state, { couponId }) => ({
  ...state,
  couponId: couponId
});

export const setCouponType = (state, { couponType }) => ({
  ...state,
  couponType: couponType
});

export const setCreditCardUsed = (state, { creditCardUsed }) => ({
  ...state,
  creditCardUsed: creditCardUsed
});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.SET_USER]: setUser,
  [Types.SET_MEALS]: setMeals,
  [Types.SET_DELIVERY_POINTS]: setDeliveryPoints,
  [Types.ADD_ITEM]: addItem,
  [Types.DELETE_ITEM]: deleteItem,
  [Types.DELETE_EXTRA]: deleteExtra,
  [Types.SET_PLACE]: setPlace,
  [Types.EXPAND_CARD]: expandCard,
  [Types.CLEAR_CART]: clearCart,
  [Types.CLEAR_DATA]: clearData,
  [Types.SET_COUPON]: setCoupon,
  [Types.SET_COUPON_ID]: setCouponId,
  [Types.SET_COUPON_TYPE]: setCouponType,
  [Types.SET_CREDIT_CARD_USED]: setCreditCardUsed
});
