import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity,
  Image
} from "react-native";
import ReduxActions from "../redux/RootRedux";
import {
  Card,
  FormValidationMessage,
  Icon,
  ListItem
} from "react-native-elements";
import { DrawerActions } from "react-navigation";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import { Hoshi } from "react-native-textinput-effects";
import Toast from "react-native-root-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class MyAccountScreen extends React.Component {
  state = {
    user: {
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      password: "",
      cards: [],
      token: ""
    },
    repeatPassword: "",
    lastnameOk: true,
    firstnameOk: true,
    emailOk: true,
    phoneOk: true,
    checkboxOk: true,
    passwordOk: true,
    repeatPasswordOk: true,
    accountAlreadyExists: false,
    impossibleToEdit: false,
    loading: false,
    editInformationSuccess: false,
    deletedCard: false,
    impossibleToDeleteCard: false
  };

  componentWillMount() {
    this.setState({
      user: {
        firstname: this.props.user.firstname,
        lastname: this.props.user.lastname,
        phone: this.props.user.phone,
        email: this.props.user.email,
        cards: this.props.user.cards,
        token: this.props.user.token,
        allergy: this.props.user.allergy,
        password: ""
      }
    });
  }

  setValue(text, key) {
    this.setState(prevState => {
      return {
        user: { ...prevState.user, [key]: text }
      };
    });
  }

  async update(user) {
    this.setState({
      accountAlreadyExists: false,
      impossibleToEdit: false,
      loading: true
    });

    try {
      // We put allergy field which is not in the form
      user.allergy = this.props.user.allergy;

      let userUpdated = await Stripe.update(user);

      AsyncStorage.mergeItem("FODUser", JSON.stringify(userUpdated), err => {
        this.setState({
          loading: false,
          editInformationSuccess: true
        });
        if (!err) {
          this.setState({
            user: userUpdated
          });
          this.props.setUser(userUpdated);
        }
      });
    } catch (error) {
      if (parseInt(error.message) === 409) {
        this.setState({
          accountAlreadyExists: true,
          loading: false
        });
      } else {
        this.setState({
          impossibleToEdit: true,
          loading: false
        });
      }
    }
  }

  deleteCreditCard = async cardId => {
    this.setState({
      deletedCard: false,
      impossibleToDeleteCard: false,
      loading: true
    });

    try {
      let userUpdated = await Stripe.deleteCard(this.props.user, cardId);

      AsyncStorage.mergeItem("FODUser", JSON.stringify(userUpdated), err => {
        this.setState({
          loading: false,
          deletedCard: true
        });
        if (!err) {
          this.setState({
            user: userUpdated
          });
          this.props.setUser(userUpdated);
        }
      });
    } catch (error) {
      this.setState({
        impossibleToDeleteCard: true,
        loading: false
      });
    }
  };

  validateInfos() {
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const nameRegexp = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{1,50}$/;
    const phoneRegexp = /^(0|\\+33|0033)[1-9][0-9]{8}$/;
    const passwordRegexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;

    let infos = {
      lastnameOk: true,
      firstnameOk: true,
      emailOk: true,
      phoneOk: true,
      passwordOk: true,
      repeatPasswordOk: true
    };

    this.setState({
      accountAlreadyExists: false,
      impossibleToEdit: false,
      editInformationSuccess: false,
      deletedCard: false,
      impossibleToDeleteCard: false
    });

    const { user } = this.state;

    // Validate data
    if (!user.firstname.match(nameRegexp)) {
      infos.firstnameOk = false;
    }
    if (!user.lastname.match(nameRegexp)) {
      infos.lastnameOk = false;
    }
    if (!user.email.match(emailRegexp)) {
      infos.emailOk = false;
    }
    if (!user.phone.match(phoneRegexp)) {
      infos.phoneOk = false;
    }
    if (user.password !== "" && !user.password.match(passwordRegexp)) {
      infos.passwordOk = false;
    }
    if (user.password !== "" && user.password !== this.state.repeatPassword) {
      infos.repeatPasswordOk = false;
    }

    this.setState({ ...infos });

    // If everything is ok
    if (
      infos.firstnameOk &&
      infos.lastnameOk &&
      infos.emailOk &&
      infos.phoneOk &&
      infos.passwordOk &&
      infos.repeatPasswordOk
    ) {
      this.update(user);
    }
  }

  static navigationOptions = ({ navigation, tintColor }) => {
    return {
      title: "Menu",
      drawerLabel: "Menu",
      drawerIcon: (
        <Icon
          type="material"
          name="account-circle"
          style={{ fontSize: 24, color: tintColor }}
        />
      ),
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      },
      headerLeft: (
        <View style={{ paddingLeft: 11 }}>
          <Icon
            type="entypo"
            name="menu"
            color="white"
            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
          />
        </View>
      )
    };
  };

  /**
   * Get icon for each credit card brand
   *
   * @param {*} brand
   */
  getIconCreditCard(brand) {
    let iconCreditCard = null;

    switch (brand) {
      case "American Express":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_amex.png")}
          />
        );
        break;

      case "Diners Club":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_diners.png")}
          />
        );
        break;

      case "Discover":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_discover.png")}
          />
        );
        break;

      case "JCB":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_jcb.png")}
          />
        );
        break;

      case "MasterCard":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_mastercard.png")}
          />
        );
        break;

      case "Visa":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_visa.png")}
          />
        );
        break;

      default:
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_unknown.png")}
          />
        );
        break;
    }

    return iconCreditCard;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <KeyboardAwareScrollView style={styles.form}>
          <Card containerStyle={styles.card}>
            <Hoshi
              label={"Prénom"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={1}
              inputPadding={16}
              maxLength={50}
              autoCorrect={false}
              autoCapitalize="words"
              onChangeText={text => this.setValue(text, "firstname")}
              defaultValue={this.props.user.firstname}
            />
            {!this.state.firstnameOk && (
              <FormValidationMessage>Prénom invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Nom"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={1}
              inputPadding={16}
              maxLength={50}
              autoCorrect={false}
              autoCapitalize="words"
              onChangeText={text => this.setValue(text, "lastname")}
              defaultValue={this.props.user.lastname}
            />
            {!this.state.lastnameOk && (
              <FormValidationMessage>Nom invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Numéro de téléphone"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={1}
              inputPadding={16}
              maxLength={10}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="phone-pad"
              onChangeText={text => this.setValue(text, "phone")}
              defaultValue={this.props.user.phone}
            />
            {!this.state.phoneOk && (
              <FormValidationMessage>Numéro invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Adresse e-mail"}
              borderColor={"#ff6600"}
              borderHeight={1}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              editable={false}
              keyboardType="email-address"
              onChangeText={text => this.setValue(text, "email")}
              defaultValue={this.props.user.email}
            />
            {!this.state.emailOk && (
              <FormValidationMessage>
                Adresse e-mail invalide
              </FormValidationMessage>
            )}
            <Hoshi
              label={"Mot de passe"}
              borderColor={"#ff6600"}
              borderHeight={1}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={text => this.setValue(text, "password")}
            />
            {!this.state.passwordOk && (
              <FormValidationMessage>
                Mot de passe de 8 caractères, majuscules, minuscules et chiffres
              </FormValidationMessage>
            )}
            <Hoshi
              label={"Répétez mot de passe"}
              borderColor={"#ff6600"}
              borderHeight={1}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={text => this.setState({ repeatPassword: text })}
            />
            {!this.state.repeatPasswordOk && (
              <FormValidationMessage>
                Mot de passe non identique
              </FormValidationMessage>
            )}
            <Toast
              visible={this.state.deletedCard}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="green"
            >
              Carte de paiement supprimée
            </Toast>
            {this.state.accountAlreadyExists && (
              <FormValidationMessage>
                Compte déjà existant
              </FormValidationMessage>
            )}
            <Toast
              visible={this.state.impossibleToEdit}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Erreur lors de la modification
            </Toast>
            <Toast
              visible={this.state.editInformationSuccess}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="green"
              position={100}
            >
              Informations enregistrées
            </Toast>
            <Loader
              loading={this.state.loading}
              text="Modification en cours ..."
            />
            <TouchableOpacity
              style={styles.button}
              onPress={this.validateInfos.bind(this)}
            >
              <Text style={styles.buttonText}>Enregister</Text>
            </TouchableOpacity>
          </Card>
          <Card
            title="Modes de paiement"
            containerStyle={styles.card}
            wrapperStyle={{ padding: 0 }}
          >
            {this.props.user.cards.length === 0 && (
              <ListItem
                style={styles.noCreditCard}
                title="Aucun moyen de paiement"
                hideChevron={true}
              />
            )}
            {this.props.user.cards.length > 0 &&
              this.props.user.cards.map(
                card =>
                  card.type == "card" && (
                    <ListItem
                      key={card.id}
                      leftIcon={
                        card.card.brand !== null
                          ? this.getIconCreditCard(card.card.brand)
                          : ""
                      }
                      rightIcon={{ name: "delete-forever" }}
                      onPressRightIcon={() => this.deleteCreditCard(card.id)}
                      title={"•••• " + card.card.last4}
                      subtitle={
                        card.owner !== null && card.owner.name !== null
                          ? card.owner.name
                          : ""
                      }
                    />
                  )
              )}
          </Card>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    padding: 20
  },
  greeting: {
    textAlign: "center"
  },
  button: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    marginTop: 20,
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
    alignSelf: "center"
  },
  form: {
    alignSelf: "center",
    padding: 10
  },
  input: {
    width: 300,
    color: "black"
  },
  inputDisabled: {
    width: 300,
    color: "grey"
  },
  label: {
    color: "black"
  },
  lastInput: {
    flex: 1,
    marginBottom: 10
  },
  buttonCancel: {
    marginTop: 10,
    marginBottom: 30,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  buttonDelete: {
    color: "red",
    marginTop: 10,
    marginBottom: 30,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  textSuccess: {
    color: "green",
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  card: {
    borderRadius: 5,
    overflow: "hidden"
  },
  iconCreditCard: {
    width: 48,
    height: 31
  },
  noCreditCard: {
    alignSelf: "center",
    justifyContent: "center",
    textAlign: "center"
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyAccountScreen);
