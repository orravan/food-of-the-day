import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View, ScrollView, Text, Picker } from "react-native";
import { Icon, List, ListItem } from "react-native-elements";
import { DrawerActions } from "react-navigation";
import moment from "moment/min/moment-with-locales";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import OrderDetail from "../components/OrderDetail";
import ModalSelector from "react-native-modal-selector";

class MyOrdersScreen extends React.Component {
  state = {
    orders: [],
    loading: true,
    month: null,
    year: null,
    pickersMonth: [],
    pickersYears: []
  };

  /**
   * Current year
   */
  currentYear = moment().format("YYYY");

  /**
   * Year of FOD creation
   */
  minYear = 2018;

  /**
   * Constructor
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    moment.locale("fr");
    this.expandOrderDetail = this.expandOrderDetail.bind(this);
    this.getAllOrders();
  }

  /**
   * Navigation Options
   */
  static navigationOptions = ({ navigation, tintColor }) => {
    return {
      title: "Menu",
      drawerLabel: "Menu",
      drawerIcon: (
        <Icon
          type="material"
          name="account-circle"
          style={{ fontSize: 24, color: tintColor }}
        />
      ),
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      },
      headerLeft: (
        <View style={{ paddingLeft: 11 }}>
          <Icon
            type="entypo"
            name="menu"
            color="white"
            onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
          />
        </View>
      )
    };
  };

  /**
   * Call Lambda to get all orders before mount component
   */
  async getAllOrders() {
    this.setState({
      loading: true
    });

    try {
      let orders = await Stripe.listOrders(this.props.user);

      this.setState({
        loading: false,
        orders: orders
      });
    } catch (error) {
      this.setState({
        loading: false
      });
    }
  }

  /**
   * Init years pickers
   */
  componentWillMount() {
    let pickersYears = [{ key: "", label: "Année" }];

    // For year list
    for (var y = this.minYear; y <= this.currentYear; y++) {
      let year = {
        key: y.toString(),
        label: y.toString()
      };

      pickersYears.push(year);
    }

    // For month list
    const pickersMonth = [
      { key: "", label: "Mois" },
      { key: "1", label: "Janvier" },
      { key: "2", label: "Février" },
      { key: "3", label: "Mars" },
      { key: "4", label: "Avril" },
      { key: "5", label: "Mai" },
      { key: "6", label: "Juin" },
      { key: "7", label: "Juillet" },
      { key: "8", label: "Août" },
      { key: "9", label: "Septembre" },
      { key: "10", label: "Octobre" },
      { key: "11", label: "Novembre" },
      { key: "12", label: "Décembre" }
    ];

    this.setState({
      pickersMonth: pickersMonth,
      pickersYears: pickersYears
    });
  }

  /**
   * Get avatar depends order status
   *
   * @param {*} orderStatus
   */
  getAvatar(orderStatus) {
    let avatar = "";

    switch (orderStatus) {
      case "fulfilled":
        avatar = require("../images/icons/fulfilled.png");
        break;

      case "paid":
        avatar = require("../images/icons/in-progress.png");
        break;

      case "returned":
        avatar = require("../images/icons/refund.png");
        break;

      case "canceled":
        avatar = require("../images/icons/refund.png");
        break;

      default:
        break;
    }

    return avatar;
  }

  getTranslatedStatus(orderStatus) {
    let translatedStatus = "";

    switch (orderStatus) {
      case "fulfilled":
        translatedStatus = "Livré";
        break;

      case "paid":
        translatedStatus = "En préparation";
        break;

      case "returned":
        translatedStatus = "Remboursé";
        break;

      case "canceled":
        translatedStatus = "Annulé";
        break;

      default:
        break;
    }

    return translatedStatus;
  }

  /**
   * Sort orders by month and year
   *
   * @param {*} month
   * @param {*} year
   */
  sortOrdersByDate(month, year) {
    let orderSortedByDate = this.state.orders.map(order => {
      if (null !== month && null !== year) {
        order.display =
          (moment(order.shippingDate, "ddd D MMM YYYY").format("M") ===
            month.toString() &&
            moment(order.shippingDate, "ddd D MMM YYYY").format("YYYY") ===
              year.toString()) ||
          (month === "" && year === "");
      }

      return order;
    });

    this.setState({
      orders: orderSortedByDate,
      month: null !== month ? month : this.state.month,
      year: null !== year ? year : this.state.year
    });
  }

  /**
   * Expand order details
   *
   * @param {*} index
   */
  expandOrderDetail(index) {
    let orders = this.state.orders;

    if (null !== index) {
      orders[index].expanded = !orders[index].expanded;

      this.setState({
        orders: orders
      });
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          <View style={styles.filter}>
            <Icon
              type="font-awesome"
              name="calendar"
              color="#515151"
              iconStyle={styles.calendar}
            />
            <ModalSelector
              initValue=""
              style={styles.listMonth}
              data={this.state.pickersMonth}
              onChange={option =>
                this.sortOrdersByDate(option.key, this.state.year)
              }
            />
            <ModalSelector
              initValue=""
              style={styles.listYear}
              data={this.state.pickersYears}
              onChange={option =>
                this.sortOrdersByDate(this.state.month, option.key)
              }
            />
          </View>
          <View style={styles.cardContainer}>
            <ScrollView style={{ flex: 1 }}>
              {this.state.orders.length > 0 ? (
                <List containerStyle={{ marginBottom: 20 }}>
                  {this.state.orders.map(
                    (order, index) =>
                      order.display && (
                        <View style={{ flex: 1 }} key={index}>
                          <ScrollView style={{ flex: 1 }}>
                            <ListItem
                              roundAvatar
                              key={order.shippingDate}
                              title={order.shippingDate}
                              rightTitle="Voir"
                              avatar={this.getAvatar(order.orders[0].status)}
                              subtitle={this.getTranslatedStatus(
                                order.orders[0].status
                              )}
                              onPress={() => this.expandOrderDetail(index)}
                            />
                            <OrderDetail
                              expanded={order.expanded}
                              orders={order.orders}
                              key={index}
                            />
                          </ScrollView>
                        </View>
                      )
                  )}
                </List>
              ) : (
                <Text style={styles.noOrders}>Aucune commande</Text>
              )}
              <Loader
                loading={this.state.loading}
                text="Chargement des commandes ..."
              />
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    margin: 20
  },
  noOrders: {
    justifyContent: "center",
    alignSelf: "center"
  },
  icon: {
    left: 10,
    right: 10,
    top: 10,
    width: 60,
    height: 40
  },
  filter: {
    flex: 1,
    minHeight: 50,
    shadowOffset: {
      height: 15
    },
    shadowColor: "rgba(50,50,93,.5)",
    backgroundColor: "white",
    shadowRadius: 15,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  calendar: {
    left: 30,
    right: 10,
    top: 15,
    width: 60,
    height: 50
  },
  listMonth: {
    top: 10,
    height: 40,
    width: 120
  },
  listYear: {
    top: 10,
    left: 10,
    height: 40,
    width: 120
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

export default connect(mapStateToProps)(MyOrdersScreen);
