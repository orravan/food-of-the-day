import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  AsyncStorage
} from "react-native";
import { FormValidationMessage, Card } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import Toast from "react-native-root-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class AllergyScreen extends React.Component {
  state = {
    allergy: "",
    errorAllergy: false,
    impossibleToSave: false,
    loading: false
  };

  /**
   * Update user informations with allergy
   */
  async update(user) {
    this.setState({
      impossibleToSave: false,
      loading: true
    });

    try {
      let userUpdated = await Stripe.update(user);

      AsyncStorage.mergeItem("FODUser", JSON.stringify(userUpdated), err => {
        this.setState({
          loading: false
        });
        if (!err) {
          this.props.navigation.navigate("Menu");
        }
      });
    } catch (error) {
      this.setState({
        impossibleToSave: true,
        loading: false
      });
    }
  }

  /**
   * Check that the form is correct
   */
  validateInfos() {
    const allergyRegexp = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{1,100}$/;

    this.setState({
      errorAllergy: false
    });

    if (this.state.allergy !== "" && !this.state.allergy.match(allergyRegexp)) {
      this.setState({
        errorAllergy: true
      });
    }

    if (!this.state.errorAllergy) {
      let user = this.props.user;

      user.allergy = this.state.allergy;
      this.update(user);
    }
  }

  render() {
    return (
      <KeyboardAwareScrollView style={styles.allergyContainer}>
        <Card containerStyle={styles.card} wrapperStyle={{ padding: 0 }}>
          <Text style={styles.textAllergy}>
            Soucieux de votre santé, nous préparons nos plats en prenant en
            compte vos allergies. Précisez vos allergies ci-dessous et nous
            remplacerons les aliments que vous ne pouvez pas manger.
          </Text>
          <TextInput
            editable={true}
            maxLength={100}
            multiline={true}
            numberOfLines={4}
            style={styles.inputAllery}
            defaultValue={this.props.user.allergy}
            onChangeText={text =>
              this.setState({
                allergy: text
              })
            }
          />
          {this.state.errorAllergy && (
            <FormValidationMessage>
              Caractères alphabétiques autorisés uniquement
            </FormValidationMessage>
          )}
          <Toast
            visible={this.state.impossibleToSave}
            shadow={false}
            animation={true}
            hideOnPress={true}
            backgroundColor="red"
            position={100}
          >
            Erreur lors de l'enregistrement des allergies
          </Toast>
          <Loader loading={this.state.loading} text="Enregistrement ..." />
        </Card>
        <TouchableOpacity
          style={styles.button}
          onPress={this.validateInfos.bind(this)}
        >
          <Text style={styles.buttonText}>Enregister</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  allergyContainer: {
    flex: 1
  },
  textAllergy: {
    flex: 0.9,
    fontSize: 13,
    marginTop: 20,
    marginBottom: 20
  },
  button: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    marginTop: 20,
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
    alignSelf: "center"
  },
  inputAllery: {
    borderWidth: 1,
    paddingLeft: 15,
    paddingRight: 15,
    marginBottom: 20,
    height: 70
  },
  card: {
    borderRadius: 5,
    overflow: "hidden",
    paddingBottom: 0
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AllergyScreen);
