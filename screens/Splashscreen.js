import React from "react";
import {
  StyleSheet,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  AppState
} from "react-native";
import ReduxActions from "../redux/RootRedux";
import Contentful from "../services/Contentful";
import { connect } from "react-redux";

import splash from "../images/splashscreen.jpg";

const { width, height } = Dimensions.get("window");

class Splashscreen extends React.Component {
  componentWillMount() {
    this.getMeals();
    this.getDeliveryPoints();
    this.checkForUserData();

    setTimeout(() => {
      this.props.navigation.navigate("Menu");
    }, 2000);
  }

  async checkForUserData() {
    const user = await AsyncStorage.getItem("FODUser");
    // If not first visit, set the user in redux and go to meals
    if (user) {
      this.props.setUser(JSON.parse(user));
    }
  }

  async getMeals() {
    const meals = await Contentful.getMeals();
    this.props.setMeals(meals);
  }

  async getDeliveryPoints() {
    const deliveryPoints = await Contentful.getDeliveryPoints();
    this.props.setDeliveryPoints(deliveryPoints);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={{ width, height }} resizeMode="cover" source={splash} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user)),
    setMeals: meals => dispatch(ReduxActions.setMeals(meals)),
    setDeliveryPoints: deliveryPoints =>
      dispatch(ReduxActions.setDeliveryPoints(deliveryPoints))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Splashscreen);
