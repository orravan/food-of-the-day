import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View, ScrollView, Text } from "react-native";
import moment from "moment/min/moment-with-locales";
import {
  Card,
  Icon,
  Divider,
  Button,
  FormInput,
  FormValidationMessage
} from "react-native-elements";
import _ from "lodash";
import numbro from "numbro";
import Loader from "../components/Loader";
import Stripe from "../services/Stripe";
import ReduxActions from "../redux/RootRedux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-root-toast";

class ValidateOrderScreen extends React.Component {
  state = {
    cartItems: [],
    totalPrice: 0,
    impossibleToPayOrder: false,
    loading: false,
    checkCoupon: false,
    invalidCoupon: false,
    coupon: ""
  };

  constructor(props) {
    super(props);

    moment.locale("fr");
  }

  componentDidMount() {
    // Total to pay
    let totalPrice = 0;

    // We group items by shipping date
    let itemsByShippingDate = _.groupBy(this.props.cart, function(o) {
      return moment(o.fields.shippingDate)
        .startOf("day")
        .format();
    });

    let cartItems = _.chain(itemsByShippingDate)
      .map(function(items, shippingDate) {
        let cartItemsFormatted = [];

        items.map(meal => {
          const index = _.findIndex(cartItemsFormatted, ci => {
            return ci.id === meal.sys.id;
          });
          if (index === -1) {
            cartItemsFormatted.push({
              id: meal.sys.id,
              title: meal.fields.title,
              price: meal.fields.price,
              allergy: meal.allergy ? meal.allergy : "",
              quantity: 1
            });
          } else {
            cartItemsFormatted[index].quantity += 1;
          }

          if (meal.extra) {
            meal.extra.map(e => {
              const extraIndex = _.findIndex(cartItemsFormatted, ci => {
                return ci.id === e.id;
              });
              if (extraIndex === -1) {
                cartItemsFormatted.push({
                  id: e.id,
                  title: e.title,
                  price: e.price,
                  quantity: 1
                });
              } else {
                cartItemsFormatted[extraIndex].quantity += 1;
              }
            });
          }
        });

        totalPrice += _.sumBy(cartItemsFormatted, ci => {
          return ci.quantity * ci.price;
        });

        return {
          shippingDate: shippingDate,
          items: cartItemsFormatted
        };
      })
      .sortBy("shippingDate")
      .value();

    this.setState({
      cartItems: cartItems,
      totalPrice: totalPrice
    });
  }

  getItemsToPay(item) {
    let itemsToPay = [];

    if (item.items) {
      _.map(item.items, function(itemToPay) {
        itemsToPay.push({
          quantity: itemToPay.quantity,
          allergy: itemToPay.allergy,
          id: itemToPay.id
        });
      });
    }

    return itemsToPay;
  }

  calculateTotalPriceWithCoupon(totalToPay) {
    if (this.props.coupon !== "" && this.props.couponType !== null) {
      let totalPriceWithCoupon = 0;

      if (this.props.couponType === "€") {
        totalPriceWithCoupon = totalToPay - parseInt(this.props.coupon);
      } else if (this.props.couponType === "%") {
        totalPriceWithCoupon =
          totalToPay - (totalToPay * parseInt(this.props.coupon)) / 100;
      }

      return totalPriceWithCoupon;
    }
  }
  payOrder = async () => {
    this.setState({
      impossibleToPayOrder: false,
      loading: true
    });

    try {
      ordersCreated = await Promise.all(
        this.state.cartItems.map(async cartItem => {
          let cartItemsTopay = this.getItemsToPay(cartItem);

          return await Stripe.payOrder(
            moment(cartItem.shippingDate)
              .format("ddd D MMM YYYY")
              .toUpperCase(),
            this.props.place.name,
            cartItemsTopay,
            this.props.user,
            this.props.couponId,
            this.props.creditCardUsed
          );
        })
      );

      this.setState({
        impossibleToPayOrder: false,
        loading: false
      });
      this.props.clearCart();
      this.props.navigation.navigate("Acknowledgment", {
        ordersCreated: ordersCreated
      });
    } catch (error) {
      this.setState({
        impossibleToPayOrder: true,
        loading: false
      });
    }
  };

  checkCoupon = async () => {
    this.setState({
      invalidCoupon: false,
      checkCoupon: true
    });

    try {
      let couponResponse = await Stripe.checkCoupon(
        this.state.coupon,
        this.props.user
      );

      // In case of coupon is an amount : multiply the amount
      // by the number of delivery date
      let coupon =
        couponResponse.amount !== null
          ? (couponResponse.amount * this.state.cartItems.length) / 100
          : couponResponse.percent;

      let couponType = couponResponse.amount !== null ? "€" : "%";
      let couponId = couponResponse.id !== null ? couponResponse.id : null;

      this.setState({
        invalidCoupon: false,
        checkCoupon: false
      });

      this.props.setCouponId(couponId);
      this.props.setCoupon(coupon);
      this.props.setCouponType(couponType);
    } catch (error) {
      this.setState({
        invalidCoupon: true,
        checkCoupon: false
      });
    }
  };
  render() {
    return (
      <KeyboardAwareScrollView style={{ flex: 1 }}>
        {this.state.cartItems.map((cartItem, index) => (
          <Card
            key={index}
            containerStyle={styles.card}
            wrapperStyle={{ padding: 0 }}
          >
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Icon
                type="font-awesome"
                name="calendar"
                color="#ff6600"
                iconStyle={styles.icon}
              />
              <Text style={styles.dateAndPlaceItems}>
                {moment(cartItem.shippingDate)
                  .format("ddd D MMM")
                  .toUpperCase()}
              </Text>
            </View>
            <Divider style={styles.divider} />
            {this.props.place && (
              <View>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Icon
                    type="entypo"
                    name="location"
                    color="#ff6600"
                    iconStyle={styles.icon}
                  />
                  <Text style={styles.dateAndPlaceItems}>
                    {this.props.place.name}
                  </Text>
                </View>
                <Text style={styles.placeAddressItems}>
                  {this.props.place.addressName}
                </Text>
              </View>
            )}
            <Divider style={styles.divider} />
            {cartItem.items &&
              cartItem.items.map(ci => (
                <View key={ci.id} style={{ flex: 1, flexDirection: "row" }}>
                  <View style={styles.itemTitle}>
                    <Text>
                      {ci.quantity} x {ci.title}
                    </Text>
                  </View>
                  <View style={styles.itemPrice}>
                    <Text style={styles.itemText}>
                      {numbro(ci.quantity * ci.price).format({ mantissa: 2 })} €
                    </Text>
                  </View>
                </View>
              ))}
          </Card>
        ))}
        <Card containerStyle={styles.card}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Text style={styles.labelCoupon}>Code de réduction :</Text>
            {this.props.coupon === "" && (
              <FormInput
                autoCapitalize="words"
                autoCorrect={false}
                placeholder="Code"
                placeholderTextColor="#808080"
                inputStyle={styles.inputCoupon}
                maxLength={20}
                onChangeText={text => this.setState({ coupon: text })}
              />
            )}
            {this.props.coupon !== "" && (
              <Text style={styles.couponUsed}>
                {" "}
                - {this.props.coupon} {this.props.couponType}
              </Text>
            )}
          </View>
          {this.props.coupon === "" && (
            <View style={{ flex: 1 }}>
              <Button
                buttonStyle={styles.buttonCoupon}
                title="Vérifier"
                onPress={this.checkCoupon}
              />
            </View>
          )}
          <Toast
            visible={this.state.invalidCoupon}
            shadow={false}
            animation={true}
            hideOnPress={true}
            backgroundColor="red"
          >
            Code de réduction invalide
          </Toast>
        </Card>
        <Card containerStyle={styles.card}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Text style={styles.totalToPay}>Total à payer :</Text>
            <Text
              style={[
                styles.amountToPay,
                this.props.coupon !== "" && styles.priceCrossed
              ]}
            >
              {numbro(this.state.totalPrice).format({ mantissa: 2 })} €
            </Text>
          </View>
          {this.props.coupon !== "" && (
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text style={styles.totalToPay} />
              <Text style={styles.amountToPay}>
                {numbro(
                  this.calculateTotalPriceWithCoupon(this.state.totalPrice)
                ).format({ mantissa: 2 })}{" "}
                €
              </Text>
            </View>
          )}
        </Card>
        <Toast
          visible={this.state.impossibleToPayOrder}
          shadow={false}
          animation={true}
          hideOnPress={true}
          backgroundColor="red"
        >
          Erreur lors du paiement de la commande
        </Toast>
        <Loader loading={this.state.loading} text="Paiement en cours ..." />
        <Loader
          loading={this.state.checkCoupon}
          text="Vérification du code ..."
        />
        <View style={{ flex: 1 }}>
          <Button
            buttonStyle={styles.button}
            title="Payer"
            onPress={this.payOrder}
          />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  priceCrossed: {
    color: "red",
    textDecorationLine: "line-through",
    textDecorationStyle: "solid"
  },
  inputCoupon: {
    width: 100
  },
  cardContainer: {
    flex: 1,
    margin: 20
  },
  button: {
    alignSelf: "center",
    backgroundColor: "green",
    marginTop: 20,
    height: 50,
    alignSelf: "stretch",
    padding: 10
  },
  buttonCoupon: {
    alignSelf: "center",
    backgroundColor: "#ff6600",
    height: 50,
    alignSelf: "stretch"
  },
  icon: {
    left: 10,
    right: 10,
    top: 10,
    width: 60,
    height: 40
  },
  dateAndPlaceItems: {
    fontSize: 15,
    fontWeight: "bold",
    width: 300,
    height: 40,
    top: 10
  },
  labelCoupon: {
    fontSize: 15,
    fontWeight: "bold",
    flex: 0.9,
    flexDirection: "row",
    alignSelf: "flex-start",
    marginTop: 15
  },
  totalToPay: {
    fontSize: 15,
    fontWeight: "bold",
    flex: 0.8,
    flexDirection: "row",
    alignSelf: "flex-start"
  },
  couponUsed: {
    fontSize: 15,
    marginTop: 15,
    fontWeight: "bold",
    flex: 0.2,
    flexDirection: "row",
    alignSelf: "flex-start",
    color: "red"
  },
  amountToPay: {
    fontSize: 17,
    fontWeight: "bold",
    flex: 0.2,
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  placeAddressItems: {
    width: 200,
    fontSize: 12,
    marginBottom: 10,
    left: 60
  },
  divider: {
    backgroundColor: "grey",
    marginBottom: 10,
    width: "50%",
    alignSelf: "center",
    height: 2
  },
  itemTitle: {
    flex: 0.8,
    flexDirection: "row",
    alignSelf: "flex-start"
  },
  itemPrice: {
    flex: 0.2,
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  itemText: {
    textAlign: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  card: {
    borderRadius: 5,
    overflow: "hidden"
  }
});

const mapStateToProps = state => {
  return {
    meals: state.root.meals,
    place: state.root.place,
    user: state.root.user,
    cart: state.root.cart,
    coupon: state.root.coupon,
    couponId: state.root.couponId,
    couponType: state.root.couponType,
    creditCardUsed: state.root.creditCardUsed
  };
};

const mapDispatchToProps = dispatch => {
  return {
    clearCart: () => dispatch(ReduxActions.clearCart()),
    setCoupon: coupon => dispatch(ReduxActions.setCoupon(coupon)),
    setCouponId: couponId => dispatch(ReduxActions.setCouponId(couponId)),
    setCouponType: couponType =>
      dispatch(ReduxActions.setCouponType(couponType))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ValidateOrderScreen);
