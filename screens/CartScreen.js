import React from "react";
import { connect } from "react-redux";
import { View, Text, StyleSheet } from "react-native";
import { Icon, Divider, Button } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";
import _ from "lodash";
import numbro from "numbro";

class CartScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: <Icon name="close" onPress={() => navigation.goBack()} />,
      headerStyle: {
        marginRight: 20
      }
    };
  };

  state = {
    cartItems: []
  };

  componentDidMount() {
    this.formatCartItems();
  }

  getTotalPrice() {
    return _.sumBy(this.state.cartItems, ci => {
      return ci.quantity * ci.price;
    });
  }

  checkUserLoggedIn() {
    if (this.props.user.token !== "") {
      this.props.navigation.navigate("Delivery");
    } else {
      this.props.navigation.navigate("Login");
    }
  }

  formatCartItems() {
    let cartItems = [];
    this.props.cart.map(meal => {
      const index = _.findIndex(cartItems, ci => {
        return ci.id === meal.sys.id;
      });
      if (index === -1) {
        cartItems.push({
          id: meal.sys.id,
          title: meal.fields.title,
          price: meal.fields.price,
          quantity: 1,
          parent: null,
          extra: false
        });
      } else {
        cartItems[index].quantity += 1;
      }

      if (meal.extra) {
        meal.extra.map(e => {
          const extraIndex = _.findIndex(cartItems, ci => {
            return ci.id === e.id;
          });
          if (extraIndex === -1) {
            cartItems.push({
              id: e.id,
              title: e.title,
              price: e.price,
              quantity: 1,
              parent: meal.sys.id,
              extra: true
            });
          } else {
            cartItems[extraIndex].quantity += 1;
          }
        });
      }
    });
    this.setState({ cartItems });
  }

  deleteItem(cartItem) {
    let cartItems = this.state.cartItems;

    _.remove(cartItems, function(currentItem) {
      return (
        currentItem.id === cartItem.id || currentItem.parent === cartItem.id
      );
    });

    this.setState({
      cartItems: cartItems
    });

    if (cartItem.extra) {
      this.props.deleteExtra(cartItem);
    } else {
      this.props.deleteItem(cartItem);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.itemTitle}>
            <Text style={{ fontSize: 16 }}>Repas</Text>
          </View>
          <View style={styles.itemQuantity}>
            <Text style={{ fontSize: 16 }}>Qté</Text>
          </View>
          <View style={styles.itemPrice}>
            <Text style={{ fontSize: 16 }}>Prix</Text>
          </View>
          <View style={styles.itemDelete} />
        </View>
        {this.state.cartItems &&
          this.state.cartItems.map(ci => (
            <View key={ci.id} style={styles.item}>
              <View style={styles.itemTitle}>
                <Text style={styles.itemText}>{ci.title}</Text>
              </View>
              <View style={styles.itemQuantity}>
                <Text style={styles.itemText}>{ci.quantity}</Text>
              </View>
              <View style={styles.itemPrice}>
                <Text style={styles.itemText}>
                  {numbro(ci.quantity * ci.price).format({ mantissa: 2 })} €
                </Text>
              </View>
              <View style={styles.itemDelete}>
                <Icon
                  name="delete"
                  type="antdesign"
                  onPress={() => this.deleteItem(ci)}
                />
              </View>
            </View>
          ))}
        <Divider style={styles.divider} />
        <View style={styles.total}>
          <Text style={[styles.itemText, { fontWeight: "bold" }]}>Total</Text>
          <Text style={[styles.itemText, { fontWeight: "bold" }]}>
            {numbro(this.getTotalPrice()).format({ mantissa: 2 })} €
          </Text>
        </View>
        <Button
          raised
          backgroundColor="green"
          disabled={this.state.cartItems.length === 0}
          style={styles.button}
          disabledStyle={styles.buttonDisabled}
          onPress={() => this.checkUserLoggedIn()}
          title="Commander"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  item: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 10,
    shadowColor: "rgb(50,50,93)",
    shadowOpacity: 0.3,
    shadowRadius: 8,
    shadowOffset: {
      height: 5
    }
  },
  header: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 10
  },
  total: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 10,
    shadowColor: "rgb(50,50,93)",
    shadowOpacity: 0.3,
    shadowRadius: 8,
    shadowOffset: {
      height: 5
    }
  },
  itemText: {
    fontSize: 18
  },
  itemTitle: {
    width: "50%"
  },
  itemQuantity: {
    width: "15%",
    alignItems: "center",
    borderLeftWidth: 1,
    borderColor: "#CCC"
  },
  itemPrice: {
    width: "25%",
    alignItems: "flex-end",
    borderLeftWidth: 1,
    borderColor: "#CCC"
  },
  itemDelete: {
    width: "10%",
    alignItems: "flex-end",
    borderLeftWidth: 1,
    borderColor: "#CCC"
  },
  divider: {
    backgroundColor: "#CCC",
    height: 2,
    marginTop: 10,
    marginBottom: 20,
    width: "80%",
    alignSelf: "center"
  },
  button: {
    marginTop: 40,
    alignSelf: "center"
  },
  buttonDisabled: {
    backgroundColor: "green"
  }
});

const mapStateToProps = state => {
  return {
    cart: state.root.cart,
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteItem: item => dispatch(ReduxActions.deleteItem(item)),
    deleteExtra: extra => dispatch(ReduxActions.deleteExtra(extra))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartScreen);
