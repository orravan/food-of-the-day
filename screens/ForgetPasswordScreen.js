import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Icon, FormValidationMessage } from "react-native-elements";
import Stripe from "../services/Stripe";
import background from "../images/background.jpg";
import Loader from "../components/Loader";
import { Hoshi } from "react-native-textinput-effects";
import Toast from "react-native-root-toast";

class ForgetPasswordScreen extends React.Component {
  state = {
    user: {
      email: ""
    },
    impossibleToResetPassword: false,
    accountDoesntExist: false,
    loading: false,
    emailOk: true
  };

  setValue(text, key) {
    this.setState(prevState => {
      return {
        user: { ...prevState.user, [key]: text }
      };
    });
  }

  async resetPassword(user) {
    this.setState({
      impossibleToResetPassword: false,
      accountDoesntExist: false,
      loading: true
    });

    try {
      let successPasswordReset = await Stripe.resetPassword(user);

      this.setState({
        loading: false
      });

      this.props.navigation.navigate("Login", {
        successResetPassword: true
      });
    } catch (error) {
      if (parseInt(error.message) === 404) {
        this.setState({
          accountDoesntExist: true,
          loading: false
        });
      } else {
        this.setState({
          impossibleToResetPassword: true,
          loading: false
        });
      }
    }
  }

  validateInfos() {
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    let infos = {
      emailOk: true
    };

    this.setState({
      impossibleToResetPassword: false,
      accountDoesntExist: false,
      emailOk: true
    });

    const { user } = this.state;

    // Validate data
    if (!user.email.match(emailRegexp)) {
      infos.emailOk = false;
    }

    // If everything is ok
    if (infos.emailOk) {
      this.resetPassword(user);
    } else {
      // or display errors
      this.setState({
        emailOk: infos.emailOk
      });

      return Promise.resolve();
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1, width: Dimensions.get("window").width }}
          imageStyle={{ resizeMode: "cover" }}
          source={background}
        >
          <ScrollView
            style={[
              styles.form,
              { marginTop: Dimensions.get("window").height * 0.3 }
            ]}
          >
            <Text style={styles.title}>Réinitialisation du mot de passe</Text>
            <Text style={styles.text}>
              Entrez votre adresse email, un email vous sera envoyé contenant
              votre nouveau mot de passe temporaire.
            </Text>
            <Hoshi
              label={"Adresse email"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              inputStyle={styles.input}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={text => this.setValue(text, "email")}
            />
            {!this.state.emailOk && (
              <FormValidationMessage>E-mail invalide</FormValidationMessage>
            )}
            <Toast
              visible={this.state.accountDoesntExist}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Aucun compte existant pour cette adresse e-mail
            </Toast>
            <Toast
              visible={this.state.impossibleToRegister}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Erreur lors de la réinitialisation de votre mot de passe
            </Toast>
            <Loader loading={this.state.loading} text="Envoi en cours ..." />
            <TouchableOpacity
              style={styles.button}
              onPress={this.validateInfos.bind(this)}
            >
              <Text style={styles.buttonText}>Envoyer</Text>
            </TouchableOpacity>
          </ScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
    fontSize: 15,
    fontWeight: "bold"
  },
  text: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center",
    textAlign: "center"
  },
  button: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "green",
    marginTop: 25,
    height: 50,
    padding: 10
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  form: {
    maxWidth: 350,
    maxHeight: 250,
    alignSelf: "center",
    padding: 10,
    backgroundColor: "rgba(255, 255, 255, 0.5)"
  },
  input: {
    width: 300,
    color: "black"
  },
  label: {
    color: "black"
  },
  buttonCancel: {
    marginTop: 10,
    marginBottom: 30,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  backButton: {
    marginTop: 125,
    flexDirection: "row",
    alignItems: "flex-start",
    marginLeft: 20
  }
});

export default connect(
  null,
  null
)(ForgetPasswordScreen);
