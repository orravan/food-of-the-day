import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import { FormValidationMessage, CheckBox, Icon } from "react-native-elements";
import { Hoshi } from "react-native-textinput-effects";
import ReduxActions from "../redux/RootRedux";
import background from "../images/background.jpg";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-root-toast";

class CredentialsScreen extends React.Component {
  state = {
    user: {
      firstname: "",
      lastname: "",
      email: "",
      phone: "",
      password: ""
    },
    repeatPassword: "",
    lastnameOk: true,
    firstnameOk: true,
    emailOk: true,
    phoneOk: true,
    checkboxOk: true,
    passwordOk: true,
    repeatPasswordOk: true,
    accountAlreadyExists: false,
    impossibleToRegister: false,
    loading: false,
    dataTreatmentAgreed: true
  };

  // Simple handler qui update les valeurs du user dans le state
  setValue(text, key) {
    this.setState(prevState => {
      return {
        user: { ...prevState.user, [key]: text }
      };
    });
  }

  async register(user) {
    this.setState({
      accountAlreadyExists: false,
      impossibleToRegister: false,
      loading: true
    });

    try {
      let userRegister = await Stripe.register(user);

      AsyncStorage.setItem("FODUser", JSON.stringify(userRegister), err => {
        this.setState({
          loading: false
        });
        if (!err) {
          this.props.setUser(userRegister);
          this.props.navigation.navigate("Menu", {
            successRegister: true,
            isConnected: true
          });
        }
      });
    } catch (error) {
      if (parseInt(error.message) === 409) {
        this.setState({
          accountAlreadyExists: true,
          loading: false
        });
      } else {
        this.setState({
          impossibleToRegister: true,
          loading: false
        });
      }
    }
  }

  validateInfos() {
    const emailRegexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const nameRegexp = /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]{1,50}$/;
    const phoneRegexp = /^(0|\\+33|0033)[1-9][0-9]{8}$/;
    const passwordRegexp = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;

    let infos = {
      lastnameOk: true,
      firstnameOk: true,
      emailOk: true,
      phoneOk: true,
      passwordOk: true,
      repeatPasswordOk: true
    };

    this.setState({
      accountAlreadyExists: false,
      impossibleToRegister: false
    });

    const { user } = this.state;

    // Validate data
    if (!user.firstname.match(nameRegexp)) {
      infos.firstnameOk = false;
    }
    if (!user.lastname.match(nameRegexp)) {
      infos.lastnameOk = false;
    }
    if (!user.email.match(emailRegexp)) {
      infos.emailOk = false;
    }
    if (!user.phone.match(phoneRegexp)) {
      infos.phoneOk = false;
    }
    if (!user.password.match(passwordRegexp)) {
      infos.passwordOk = false;
    }
    if (user.password !== "" && user.password !== this.state.repeatPassword) {
      infos.repeatPasswordOk = false;
    }

    // If everything is ok
    if (
      infos.firstnameOk &&
      infos.lastnameOk &&
      infos.emailOk &&
      infos.phoneOk &&
      infos.passwordOk &&
      infos.repeatPasswordOk &&
      this.state.dataTreatmentAgreed
    ) {
      this.register(user);
    } else {
      // or display errors
      this.setState({ ...infos });
      return Promise.resolve();
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1, width: Dimensions.get("window").width }}
          imageStyle={{ resizeMode: "cover" }}
          source={background}
        >
          <KeyboardAwareScrollView
            style={[
              styles.form,
              { marginTop: Dimensions.get("window").height * 0.3 }
            ]}
          >
            <Hoshi
              label={"Prénom"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={50}
              autoCorrect={false}
              autoCapitalize="words"
              onChangeText={text => this.setValue(text, "firstname")}
            />
            {!this.state.firstnameOk && (
              <FormValidationMessage>Prénom invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Nom"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={50}
              autoCorrect={false}
              autoCapitalize="words"
              onChangeText={text => this.setValue(text, "lastname")}
            />
            {!this.state.lastnameOk && (
              <FormValidationMessage>Nom invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Numéro de téléphone"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={10}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="phone-pad"
              onChangeText={text => this.setValue(text, "phone")}
            />
            {!this.state.phoneOk && (
              <FormValidationMessage>Numéro invalide</FormValidationMessage>
            )}
            <Hoshi
              label={"Adresse email"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={text => this.setValue(text, "email")}
            />
            {!this.state.emailOk && (
              <FormValidationMessage>
                Adresse e-mail invalide
              </FormValidationMessage>
            )}
            <Hoshi
              label={"Mot de passe"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={text => this.setValue(text, "password")}
            />
            {!this.state.passwordOk && (
              <FormValidationMessage>
                Mot de passe de 8 caractères, majuscules, minuscules et chiffres
              </FormValidationMessage>
            )}
            <Hoshi
              label={"Saisissez à nouveau votre mot de passe"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={text => this.setState({ repeatPassword: text })}
            />
            {!this.state.repeatPasswordOk && (
              <FormValidationMessage>
                Mot de passe non identique
              </FormValidationMessage>
            )}
            <CheckBox
              title="J'autorise FOD à stocker mes informations personnelles et à me contacter à des fins commerciales."
              inputStyle={styles.lastInput}
              onPress={() =>
                this.setState({
                  dataTreatmentAgreed: !this.state.dataTreatmentAgreed
                })
              }
              checked={this.state.dataTreatmentAgreed}
            />
            {!this.state.dataTreatmentAgreed && (
              <FormValidationMessage>
                Veuillez cocher cette case
              </FormValidationMessage>
            )}
            <Toast
              visible={this.state.accountAlreadyExists}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Compte déjà existant
            </Toast>
            <Toast
              visible={this.state.impossibleToRegister}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Erreur lors de l'inscription
            </Toast>
            <Loader
              loading={this.state.loading}
              text="Inscription en cours ..."
            />
            <TouchableOpacity
              style={styles.button}
              onPress={this.validateInfos.bind(this)}
            >
              <Text style={styles.buttonText}>S'inscrire</Text>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    padding: 20
  },
  greeting: {
    textAlign: "center"
  },
  button: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
    alignSelf: "center"
  },
  form: {
    maxWidth: 350,
    alignSelf: "center",
    padding: 10,
    backgroundColor: "rgba(255, 255, 255, 0.5)"
  },
  input: {
    width: 300,
    color: "black"
  },
  label: {
    color: "black"
  },
  lastInput: {
    marginBottom: 50
  },
  buttonCancel: {
    marginTop: 10,
    marginBottom: 30,
    textAlign: "center",
    textDecorationLine: "underline"
  },
  backButton: {
    marginTop: 125,
    flexDirection: "row",
    alignItems: "flex-start",
    marginLeft: 20
  }
});

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(CredentialsScreen);
