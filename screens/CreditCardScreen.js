import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View, LayoutAnimation, AsyncStorage } from "react-native";
import {
  Button,
  FormValidationMessage,
  CheckBox,
  Card
} from "react-native-elements";
import { LiteCreditCardInput } from "react-native-credit-card-input";
import { Hoshi } from "react-native-textinput-effects";
import ReduxActions from "../redux/RootRedux";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import Toast from "react-native-root-toast";

class CreditCardScreen extends React.Component {
  state = {
    formValid: false,
    number: "",
    expMonth: "",
    expYear: "",
    cvc: "",
    name: "",
    impossibleToAddCard: false,
    storeCreditCard: true,
    loading: false,
    labels: {
      number: "Numéro de carte",
      expiry: "Date d'expiration",
      cvc: "Code de sécurité",
      name: "Nom du propriétaire"
    },
    placeholders: {
      number: "•••• •••• •••• ••••",
      expiry: "MM/AA",
      cvc: "CVC",
      name: "Nom du propriétaire"
    }
  };

  onChange(form) {
    if (form.valid && this.state.storeCreditCard) {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      this.setState({
        formValid: true,
        number: form.values.number,
        expMonth: form.values.expiry.split("/")[0],
        expYear: form.values.expiry.split("/")[1],
        cvc: form.values.cvc
      });
    }
  }

  addCreditCard = async () => {
    this.setState({
      impossibleToAddCard: false,
      loading: true
    });

    try {
      let cardAdded = await Stripe.addCard(
        this.state.number,
        this.state.expMonth,
        this.state.expYear,
        this.state.cvc,
        this.state.name,
        this.props.user
      );

      AsyncStorage.mergeItem("FODUser", JSON.stringify(cardAdded), err => {
        this.setState({
          loading: false
        });

        if (!err) {
          this.props.setUser(cardAdded);
          this.props.navigation.navigate("PaymentChoice", {
            selectLastCard: true
          });
        }
      });
    } catch (error) {
      this.setState({
        impossibleToAddCard: true,
        loading: false
      });
    }
  };

  render() {
    return (
      <View style={styles.cardContainer}>
        <Card containerStyle={styles.card} wrapperStyle={{ padding: 0 }}>
          <LiteCreditCardInput
            autoFocus
            labels={this.state.labels}
            placeholders={this.state.placeholders}
            cardScale={1.0}
            onChange={this.onChange.bind(this)}
          />
          <Hoshi
            label={"Nom du propriétaire"}
            // this is used as active border color
            borderColor={"#ff6600"}
            // active border height
            borderHeight={3}
            inputPadding={16}
            maxLength={20}
            autoCorrect={false}
            autoCapitalize="words"
            onChangeText={text =>
              this.setState({
                name: text
              })
            }
          />
        </Card>
        <CheckBox
          title="Autoriser le stockage de ma carte bancaire pour mes futurs achats sur FOD."
          onPress={() =>
            this.setState({
              storeCreditCard: !this.state.storeCreditCard
            })
          }
          checked={this.state.storeCreditCard}
        />
        {this.state.dataTreatmentAgreed && (
          <FormValidationMessage>
            Veuillez cocher cette case
          </FormValidationMessage>
        )}
        {this.state.formValid && this.state.name !== "" && (
          <Button
            buttonStyle={styles.button}
            onPress={this.addCreditCard}
            title="Enregister la carte"
          />
        )}
        <Toast
          visible={this.state.impossibleToAddCard}
          shadow={false}
          animation={true}
          hideOnPress={true}
          backgroundColor="red"
        >
          Erreur lors de l'ajout de la carte de paiement
        </Toast>
        <Loader
          loading={this.state.loading}
          text="Ajout carte de paiement..."
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    margin: 20
  },
  button: {
    width: 200,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    padding: 10,
    marginTop: 50
  },
  card: {
    borderRadius: 5,
    overflow: "hidden",
    padding: 2
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreditCardScreen);
