import React from "react";
import { connect } from "react-redux";
import { StyleSheet, View, AsyncStorage } from "react-native";
import ReduxActions from "../redux/RootRedux";
import { Icon } from "react-native-elements";
import OneSignal from "../services/OneSignal";

class LogoutScreen extends React.Component {
  static navigationOptions = {
    drawerIcon: ({ tintColor }) => (
      <Icon
        type="font-awesome"
        name="power"
        style={{ fontSize: 24, color: tintColor }}
      />
    )
  };

  componentWillMount() {
    this.removeDataStorage();
  }

  async removeDataStorage() {
    try {
      await AsyncStorage.removeItem("FODUser");
      OneSignal.logOut();
      this.props.clearData();
      this.props.navigation.navigate("Menu", { isConnected: false });
    } catch (exception) {
      console.log(exception);
    }
  }

  render() {
    return <View style={styles.cardContainer} />;
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    margin: 20
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user)),
    clearData: () => dispatch(ReduxActions.clearData())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LogoutScreen);
