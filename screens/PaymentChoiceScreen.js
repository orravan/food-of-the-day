import React from "react";
import { connect } from "react-redux";
import { StyleSheet, ScrollView, Image } from "react-native";
import { Button, ListItem, Card } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";

class PaymentChoiceScreen extends React.Component {
  state = {
    cards: [],
    cardSelected: null
  };

  componentDidMount() {
    const cardsLength = this.props.user.cards.length;

    if (cardsLength > 0) {
      let cards = [];
      let cardSelected = null;

      this.props.user.cards.map((card, i) => {
        if (cardsLength === i + 1) {
          cards[card.id] = true;
          cardSelected = card.id;
        } else {
          cards[card.id] = false;
        }
      });

      this.setState({
        cards: cards,
        cardSelected: cardSelected
      });
    }
  }

  /**
   * Get icon for each credit card brand
   *
   * @param {*} brand
   */
  getIconCreditCard(brand) {
    let iconCreditCard = null;

    switch (brand) {
      case "American Express":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_amex.png")}
          />
        );
        break;

      case "Diners Club":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_diners.png")}
          />
        );
        break;

      case "Discover":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_discover.png")}
          />
        );
        break;

      case "JCB":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_jcb.png")}
          />
        );
        break;

      case "MasterCard":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_mastercard.png")}
          />
        );
        break;

      case "Visa":
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_visa.png")}
          />
        );
        break;

      default:
        iconCreditCard = (
          <Image
            style={styles.iconCreditCard}
            source={require("../images/icons/cards/stp_card_unknown.png")}
          />
        );
        break;
    }

    return iconCreditCard;
  }

  /**
   * Set as true the selected credit card
   *
   * @param {*} cardId
   */
  selectCard(cardId) {
    let cards = [];

    this.props.user.cards.map(card => {
      if (cardId === card.id) {
        cards[card.id] = true;
      } else {
        cards[card.id] = false;
      }
    });

    this.setState({
      cards: cards,
      cardSelected: cardId
    });
  }

  /**
   * Submit form to select credit card and go to validate order screen
   */
  validateCard() {
    if (this.state.cardSelected !== null) {
      this.props.setCreditCardUsed(this.state.cardSelected);
      this.props.navigation.navigate("ValidateOrder");
    }
  }

  render() {
    return (
      <ScrollView style={styles.cardContainer}>
        <Card
          title="Choisir mode de paiement"
          containerStyle={styles.card}
          wrapperStyle={{ padding: 0 }}
        >
          {typeof this.props.user.cards !== "undefined" &&
            this.props.user.cards.length === 0 && (
              <ListItem title="Aucun moyen de paiement" hideChevron={true} />
            )}
          {typeof this.props.user.cards !== "undefined" &&
            this.props.user.cards.length > 0 &&
            this.props.user.cards.map(
              card =>
                card.type == "card" && (
                  <ListItem
                    key={card.id}
                    hideChevron
                    switchButton
                    switched={this.state.cards[card.id]}
                    switchDisabled={false}
                    leftIcon={
                      card.card.brand !== null
                        ? this.getIconCreditCard(card.card.brand)
                        : ""
                    }
                    onSwitch={() => this.selectCard(card.id)}
                    title={"•••• " + card.card.last4}
                    subtitle={
                      card.owner !== null && card.owner.name !== null
                        ? card.owner.name
                        : ""
                    }
                  />
                )
            )}
          <ListItem
            roundAvatar
            titleStyle={styles.addCard}
            title="Ajouter un moyen de paiement"
            leftIcon={{ name: "add-circle" }}
            hideChevron={true}
            onPress={() => this.props.navigation.navigate("CreditCard")}
          />
        </Card>
        {this.state.cardSelected !== null && (
          <Button
            buttonStyle={styles.button}
            onPress={this.validateCard.bind(this)}
            title="Valider"
          />
        )}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
    margin: 20
  },
  addCard: {
    fontSize: 13
  },
  iconCreditCard: {
    width: 48,
    height: 31
  },
  button: {
    width: 200,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    padding: 10,
    marginTop: 50
  },
  card: {
    borderRadius: 5,
    overflow: "hidden",
    paddingBottom: 0
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user)),
    setCreditCardUsed: creditCardUsed =>
      dispatch(ReduxActions.setCreditCardUsed(creditCardUsed))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentChoiceScreen);
