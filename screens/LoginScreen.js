import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import { FormValidationMessage, Icon } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";
import background from "../images/background.jpg";
import Stripe from "../services/Stripe";
import Loader from "../components/Loader";
import Toast from "react-native-root-toast";
import { Hoshi } from "react-native-textinput-effects";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

class LoginScreen extends React.Component {
  state = {
    user: {
      email: "",
      password: ""
    },
    emailOk: true,
    passwordOk: true,
    badCredentials: false,
    impossibleToLogin: false,
    loading: false
  };

  static navigationOptions = ({ navigation, tintColor }) => {
    return {
      title: "Se connecter",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      },
      headerRight: <Icon name="close" onPress={() => navigation.goBack()} />
    };
  };

  // Simple handler qui update les valeurs du user dans le state
  setValue(text, key) {
    this.setState(prevState => {
      return {
        user: { ...prevState.user, [key]: text }
      };
    });
  }

  async login(user) {
    this.setState({
      badCredentials: false,
      impossibleToLogin: false,
      loading: true
    });

    try {
      let userLoggedIn = await Stripe.login(user);

      AsyncStorage.setItem("FODUser", JSON.stringify(userLoggedIn), err => {
        this.setState({ loading: false });
        if (!err) {
          this.props.setUser(userLoggedIn);
          this.props.navigation.navigate("Menu", { isConnected: true });
        }
      });
    } catch (error) {
      if (parseInt(error.message) === 403 || parseInt(error.message) === 400) {
        this.setState({
          badCredentials: true,
          loading: false
        });
      } else {
        this.setState({
          impossibleToLogin: true,
          loading: false
        });
      }
    }
  }

  validateInfos() {
    let infos = {
      emailOk: true,
      passwordOk: true
    };
    const { user } = this.state;
    // Validate data
    if (user.email === "") {
      infos.emailOk = false;
    }

    if (user.password === "") {
      infos.passwordOk = false;
    }

    if (infos.emailOk && infos.passwordOk) {
      this.login(user);
    } else {
      // Ou afficher les problèmes
      this.setState({ ...infos });
      return Promise.resolve();
    }
  }

  render() {
    const { navigation } = this.props;
    const successResetPassword = navigation.getParam(
      "successResetPassword",
      false
    );

    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1, width: Dimensions.get("window").width }}
          imageStyle={{ resizeMode: "cover" }}
          source={background}
        >
          <KeyboardAwareScrollView
            style={[
              styles.form,
              { marginTop: Dimensions.get("window").height * 0.3 }
            ]}
          >
            <Hoshi
              label={"Adresse email"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              inputStyle={styles.input}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={text => this.setValue(text, "email")}
            />
            <Hoshi
              label={"Mot de passe"}
              // this is used as active border color
              borderColor={"#ff6600"}
              // active border height
              borderHeight={3}
              inputPadding={16}
              inputStyle={styles.input}
              maxLength={100}
              autoCorrect={false}
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={text => this.setValue(text, "password")}
            />
            {!this.state.emailOk ||
              !this.state.passwordOk ||
              (this.state.badCredentials && (
                <FormValidationMessage style={styles.error}>
                  E-mail ou mot de passe invalide
                </FormValidationMessage>
              ))}
            <Toast
              visible={this.state.impossibleToLogin}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="red"
            >
              Une erreur est survenue
            </Toast>
            <Loader
              loading={this.state.loading}
              text="Connexion en cours ..."
            />
            <TouchableOpacity
              style={styles.button}
              onPress={this.validateInfos.bind(this)}
            >
              <Text style={styles.buttonText}>Se connecter</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buttonRegister}
              onPress={() => this.props.navigation.navigate("Credentials")}
            >
              <Text style={styles.buttonText}>Inscription</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.props.navigation.navigate("ForgetPassword")}
            >
              <Text style={styles.signUp}>Mot de passe oublié</Text>
            </TouchableOpacity>
            <Toast
              visible={successResetPassword}
              shadow={false}
              animation={true}
              hideOnPress={true}
              backgroundColor="green"
            >
              Votre nouveau mot de passe temporaire vous a été envoyé
            </Toast>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textContainer: {
    padding: 20
  },
  greeting: {
    textAlign: "center"
  },
  button: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "green",
    height: 50,
    marginTop: 25,
    justifyContent: "center"
  },
  buttonRegister: {
    width: 300,
    alignSelf: "center",
    backgroundColor: "#ff6600",
    height: 50,
    padding: 10,
    marginTop: 25
  },
  buttonText: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
    alignSelf: "center"
  },
  form: {
    flex: 1,
    maxHeight: 380,
    padding: 15,
    alignSelf: "center",
    backgroundColor: "rgba(255, 255, 255, 0.5)"
  },
  input: {
    width: 275,
    color: "black"
  },
  label: {
    color: "black"
  },
  error: {
    marginBottom: 25
  },
  signUp: {
    marginTop: 20,
    fontSize: 15,
    color: "black",
    textAlign: "center",
    textDecorationLine: "underline"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  null,
  mapDispatchToProps
)(LoginScreen);
