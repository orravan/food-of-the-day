import React from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  LayoutAnimation,
  UIManager,
  Platform,
  AppState,
  Image
} from "react-native";
import ReduxActions from "../redux/RootRedux";
import { Icon, Badge, Card } from "react-native-elements";
import moment from "moment/min/moment-with-locales";
import CustomCard from "../components/CustomCard";
import { DrawerActions } from "react-navigation";
import Toast from "react-native-root-toast";
import Contentful from "../services/Contentful";

class MenuScreen extends React.Component {
  state = {
    selectedDate: 0,
    appState: AppState.currentState
  };

  componentDidMount() {
    this.toggleLoggedIn();
    AppState.addEventListener("change", this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
      this.getMeals();
      this.getDeliveryPoints();
    }
    this.setState({ appState: nextAppState });
  };

  async getMeals() {
    const meals = await Contentful.getMeals();
    this.props.setMeals(meals);
  }

  async getDeliveryPoints() {
    const deliveryPoints = await Contentful.getDeliveryPoints();
    this.props.setDeliveryPoints(deliveryPoints);
  }

  constructor(props) {
    super(props);

    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    this.handleSelectedDate = this.handleSelectedDate.bind(this);
    this.toggleLoggedIn();

    moment.locale("fr");
  }

  static navigationOptions = ({ navigation, tintColor }) => {
    return {
      title: "Menu",
      drawerLabel: "Menu",
      drawerIcon: (
        <Icon
          type="material"
          name="account-circle"
          style={{ fontSize: 24, color: tintColor }}
        />
      ),
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      },
      headerLeft: (
        <View style={{ paddingLeft: 11 }}>
          {navigation.getParam("isConnected", false) ? (
            <Icon
              type="entypo"
              name="menu"
              color="white"
              onPress={() => navigation.dispatch(DrawerActions.openDrawer())}
            />
          ) : (
            <Icon
              type="material"
              name="account-circle"
              color="white"
              onPress={() => navigation.navigate("Login")}
            />
          )}
        </View>
      )
    };
  };

  handleSelectedDate(index) {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);

    this.setState({ selectedDate: index });
  }

  countItemsCart() {
    let nbExtraItems = 0;

    this.props.cart.map(function(i) {
      if (i.extra) {
        return (nbExtraItems += i.extra.length);
      }
    });

    return nbExtraItems + this.props.cart.length;
  }

  toggleLoggedIn() {
    if (this.props.user.token !== "") {
      this.props.navigation.setParams({ isConnected: true });
    } else {
      this.props.navigation.setParams({ isConnected: false });
    }
  }

  render() {
    const { navigation } = this.props;
    const successRegister = navigation.getParam("successRegister", false);

    return (
      <View style={{ flex: 1 }}>
        <ScrollView style={{ flex: 1 }}>
          {this.props.meals && this.props.meals.length > 0 ? (
            <View>
              {this.props.expandedCard === 0 && (
                <View>
                  <View style={styles.filter}>
                    <Icon
                      type="font-awesome"
                      name="calendar"
                      color="#515151"
                      iconStyle={styles.calendar}
                    />
                    <ScrollView
                      style={styles.selectDate}
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}
                    >
                      {this.props.meals.map((dateMeal, index) => (
                        <View style={styles.buttonContainer} key={index}>
                          <TouchableHighlight
                            activeOpacity={1}
                            style={
                              this.state.selectedDate == index
                                ? styles.buttonOn
                                : styles.buttonOff
                            }
                            onPress={() => this.handleSelectedDate(index)}
                          >
                            <Text style={styles.dateFilter}>
                              {moment(dateMeal.shippingDate)
                                .format("dddd")
                                .toUpperCase()}{" "}
                              -{" "}
                              {moment(dateMeal.shippingDate)
                                .add(1, "days")
                                .format("dddd")
                                .toUpperCase()}
                            </Text>
                          </TouchableHighlight>
                        </View>
                      ))}
                    </ScrollView>
                  </View>
                  <View style={styles.limitDateCheckout}>
                    <Text style={styles.textLimitDateCheckout}>
                      Date limite pour commander :{" "}
                      <Text style={styles.dateLimit}>
                        {moment(
                          this.props.meals[this.state.selectedDate]
                            .limitDateCheckout
                        ).format("dddd D MMMM à HH:mm")}
                      </Text>
                    </Text>
                    <Text style={styles.textLimitDateCheckout}>
                      Livraison le Mardi et le Jeudi chaque semaine.
                    </Text>
                  </View>
                </View>
              )}
              <View style={styles.meals}>
                {this.props.meals.map((dateMeal, index) =>
                  dateMeal.meals.map((meal, subIndex) => {
                    let count = parseInt(index + 1 + "" + subIndex);

                    return (
                      (this.props.expandedCard === 0 ||
                        this.props.expandedCard === count + 1) &&
                      this.state.selectedDate == index && (
                        <CustomCard
                          meal={meal}
                          index={count + 1}
                          key={count}
                          navigation={this.props.navigation}
                        />
                      )
                    );
                  })
                )}
              </View>
            </View>
          ) : (
            <Card
              title="Nous revenons très bientôt avec de nouveaux menus ..."
              image={require("../images/fod.jpg")}
              wrapperStyle={{ padding: 0 }}
            >
              <View>
                <View>
                  <Text style={styles.textEmptyMeal}>
                    Suivez nos actualités sur les réseaux sociaux :
                  </Text>
                  <Image
                    source={require("../images/facebook.png")}
                    style={{ width: 50, height: 50, alignSelf: "center" }}
                  />
                  <Text style={[styles.textEmptyMeal, styles.facebook]}>
                    FOD - 34
                  </Text>
                  <Image
                    source={require("../images/instagram.png")}
                    style={{ width: 50, height: 50, alignSelf: "center" }}
                  />
                  <Text style={[styles.textEmptyMeal, styles.instagram]}>
                    fod__34
                  </Text>
                  <Text style={styles.textEmptyMeal}>
                    Livraison les Mardi et Jeudi de chaque semaine.
                  </Text>
                </View>
              </View>
            </Card>
          )}
          <View style={styles.spacer} />
        </ScrollView>
        {this.props.expandedCard === 0 && (
          <View style={styles.floatingButton}>
            <Icon
              containerStyle={styles.floatingIcon}
              raised
              size={25}
              name="shopping-cart"
              type="font-awesome"
              color="white"
              onPress={() => this.props.navigation.navigate("Cart")}
            />
            {this.props.cart.length > 0 && (
              <Badge
                containerStyle={styles.badge}
                value={this.countItemsCart()}
                textStyle={{ color: "white" }}
              />
            )}
          </View>
        )}
        <Toast
          visible={successRegister}
          shadow={false}
          animation={true}
          hideOnPress={true}
          backgroundColor="green"
          position={-100}
        >
          Félicitations, vous êtes inscrit !
        </Toast>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  floatingButton: {
    position: "absolute",
    bottom: 10,
    elevation: 10,
    zIndex: 10,
    elevation: 10,
    height: 100,
    width: "100%"
  },
  floatingIcon: {
    position: "absolute",
    alignSelf: "center",
    backgroundColor: "green",
    bottom: 0,
    shadowRadius: 5,
    shadowOffset: {
      height: 10
    }
  },
  badge: {
    backgroundColor: "#ff944d",
    position: "absolute",
    top: 30,
    zIndex: 10,
    elevation: 10,
    left: Dimensions.get("window").width / 2,
    alignSelf: "center"
  },
  spacer: {
    height: 100
  },
  shippingDate: {
    fontSize: 18,
    alignSelf: "center"
  },
  noMeal: {
    flex: 1,
    justifyContent: "center",
    alignSelf: "center"
  },
  filter: {
    flex: 1,
    minHeight: 50,
    shadowOffset: {
      height: 15
    },
    shadowColor: "rgba(50,50,93,.5)",
    backgroundColor: "white",
    shadowRadius: 15,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  limitDateCheckout: {
    flex: 1,
    minHeight: 30,
    shadowOffset: {
      height: 15
    },
    shadowColor: "rgba(50,50,93,.5)",
    backgroundColor: "white",
    shadowRadius: 15,
    justifyContent: "center",
    alignItems: "center"
  },
  textLimitDateCheckout: {
    textAlign: "center",
    alignSelf: "center",
    fontSize: 12,
    fontStyle: "italic"
  },
  dateLimit: {
    fontWeight: "bold",
    color: "#ff944d"
  },
  calendar: {
    left: 30,
    right: 10,
    top: 15,
    width: 60,
    height: 50
  },
  selectDate: {
    left: 10,
    top: 5,
    borderColor: "#515151",
    borderLeftWidth: 0.5
  },
  buttonContainer: {
    flex: 1,
    marginLeft: 10
  },
  buttonOn: {
    backgroundColor: "#ff6600",
    borderRadius: 10,
    top: 5,
    height: 40
  },
  buttonOff: {
    backgroundColor: "#ffb380",
    borderRadius: 10,
    top: 5,
    height: 40
  },
  dateFilter: {
    color: "#fff",
    fontSize: 15,
    fontWeight: "bold",
    margin: 5
  },
  meals: {
    backgroundColor: "rgba(255, 255, 255, 0.4)",
    borderRadius: 5
  },
  textEmptyMeal: {
    textAlign: "center",
    alignSelf: "center",
    fontSize: 20,
    fontWeight: "bold",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center"
  },
  facebook: {
    color: "#3b5998"
  },
  instagram: {
    color: "#C20808"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    setMeals: meals => dispatch(ReduxActions.setMeals(meals)),
    setDeliveryPoints: deliveryPoints =>
      dispatch(ReduxActions.setDeliveryPoints(deliveryPoints))
  };
};

const mapStateToProps = state => {
  return {
    meals: state.root.meals,
    expandedCard: state.root.expandedCard,
    cart: state.root.cart,
    user: state.root.user
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuScreen);
