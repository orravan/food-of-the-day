import React from "react";
import { connect } from "react-redux";
import {
  StyleSheet,
  Text,
  Image,
  LayoutAnimation,
  View,
  Dimensions
} from "react-native";
import MapView, { Marker, Callout, ProviderPropType } from "react-native-maps";
import { Button } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";
import _ from "lodash";

const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;
const LATITUDE = 43.61092;
const LONGITUDE = 3.87723;
const LATITUDE_DELTA = 0.2;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class DeliveryScreen extends React.Component {
  state = {
    place: null,
    marker: false
  };

  setPlace() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.setPlace(this.state.place);
    this.props.navigation.navigate("PaymentChoice");
  }

  constructor(props) {
    super(props);
    this.mapRef = null;

    this._onMarkerPress = e => {
      const place = e.nativeEvent;

      let coordinate = place.coordinate;
      place.latitudeDelta = LATITUDE_DELTA;
      place.longitudeDelta = LATITUDE_DELTA;

      let placeField = _.find(
        this.props.deliveryPoints,
        d => d.sys.id === place.id
      );

      this.mapRef.animateToRegion(coordinate, 1000);

      if (typeof placeField !== "undefined") {
        this.setState({ marker: true, place: placeField.fields });
      }
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          onMarkerDeselect={() => this.setState({ marker: false, place: null })}
          onMarkerSelect={this._onMarkerPress}
          ref={ref => {
            this.mapRef = ref;
          }}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
        >
          {this.props.deliveryPoints &&
            this.props.deliveryPoints.map((deliveryPoint, index) => (
              <Marker
                coordinate={{
                  latitude: deliveryPoint.fields.address.lat,
                  longitude: deliveryPoint.fields.address.lon
                }}
                key={index}
                centerOffset={{ x: -18, y: -60 }}
                anchor={{ x: 0.69, y: 1 }}
                title={deliveryPoint.fields.name}
                identifier={deliveryPoint.sys.id}
                onPress={() =>
                  this.setState({ marker: true, place: deliveryPoint.fields })
                }
              >
                <Callout>
                  <View style={styles.markerPopUp}>
                    <Text style={styles.markerTitle}>
                      {deliveryPoint.fields.name}
                    </Text>
                    <Text style={styles.markerAddress}>
                      {deliveryPoint.fields.addressName}
                    </Text>
                    <Image
                      resizeMode="cover"
                      style={styles.markerImage}
                      source={{ uri: `https:${deliveryPoint.picture}` }}
                    />
                  </View>
                </Callout>
              </Marker>
            ))}
        </MapView>
        <View style={styles.buttonContainer}>
          {this.state.marker ? (
            <Button
              raised
              icon={{ name: "check", type: "evilIcons" }}
              buttonStyle={styles.markerButton}
              backgroundColor="green"
              color="white"
              title="Valider"
              onPress={() => this.setPlace()}
            />
          ) : (
            <View style={styles.bubble}>
              <Text style={styles.button}>
                Choisissez votre lieu de livraison
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

DeliveryScreen.propTypes = {
  provider: ProviderPropType
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  button: {
    paddingHorizontal: 12,
    alignItems: "center",
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: "row",
    marginVertical: 50,
    backgroundColor: "transparent"
  },
  bubble: {
    flex: 1,
    backgroundColor: "rgba(255,255,255,0.7)",
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  markerTitle: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  markerAddress: {
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center"
  },
  markerButton: {
    width: 200,
    paddingVertical: 30,
    alignSelf: "center",
    justifyContent: "center"
  },
  markerPopUp: {
    maxWidth: 300,
    justifyContent: "center",
    alignItems: "center"
  },
  markerImage: {
    width: 70,
    height: 70,
    top: 10,
    bottom: 10
  }
});

const mapStateToProps = state => {
  return {
    expandedCard: state.root.expandedCard,
    meals: state.root.meals,
    deliveryPoints: state.root.deliveryPoints,
    user: state.root.user
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setPlace: place => dispatch(ReduxActions.setPlace(place)),
    setUser: user => dispatch(ReduxActions.setUser(user))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeliveryScreen);
