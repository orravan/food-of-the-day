import React from "react";
import { connect } from "react-redux";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Linking,
  TouchableWithoutFeedback
} from "react-native";
import { Button, Divider, Icon, Card } from "react-native-elements";
import _ from "lodash";

class AcknowledgmentScreen extends React.Component {
  state = {
    ordersCreated: [],
    ordersIdSentence: "",
    contactFod: "contact@food-of-the-day.fr"
  };

  componentDidMount() {
    let ordersCreated = this.props.navigation.getParam("ordersCreated", []);

    this.setState({
      ordersCreated: ordersCreated,
      ordersIdSentence: _.join(ordersCreated, ", ")
    });
  }

  contactFod() {
    Linking.canOpenURL("mailto:" + this.state.contactFod)
      .then(supported => {
        if (!supported) {
          console.log("Can't handle url: mailto:" + this.state.contactFod);
        } else {
          return Linking.openURL("mailto:" + this.state.contactFod);
        }
      })
      .catch(err => console.error("An error occurred", err));
  }

  static navigationOptions = ({ navigation, tintColor }) => {
    return {
      title: "Commande validée",
      headerStyle: {
        backgroundColor: "#ff6600"
      },
      headerTintColor: "#fff",
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        alignSelf: "center",
        justifyContent: "center"
      },
      headerLeft: null
    };
  };

  render() {
    return (
      <ScrollView>
        <Card containerStyle={styles.card}>
          {this.state.ordersCreated.length > 1 ? (
            <Text style={styles.orderValidated}>
              Vos commandes n° {this.state.ordersIdSentence} sont validées.{" "}
            </Text>
          ) : (
            <Text style={styles.orderValidated}>
              Votre commande n°{this.state.ordersIdSentence} est validée.{" "}
            </Text>
          )}
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Icon
              type="entypo"
              name="info-with-circle"
              color="#ff6600"
              iconStyle={styles.icon}
            />
            <Text style={styles.text}>
              Votre repas sera déposé dans les réfrigérateurs de votre point de
              livraison entre 9h et 12h le jour de la livraison. Veuillez vous
              adresser à l'accueil du point de livraison pour récupérer votre
              commande.
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Icon
              type="materialicons"
              name="email"
              color="#ff6600"
              iconStyle={styles.icon}
            />
            <Text style={styles.text}>
              Vous recevrez un e-mail lorsque votre repas sera livré.
            </Text>
          </View>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Text style={styles.text}>
              Merci de nous faire confiance et bon FOD !
            </Text>
          </View>
          <Divider style={styles.divider} />
          <View style={{ flex: 1, flexDirection: "row" }}>
            <Icon
              type="materialicons"
              name="question-answer"
              color="#ff6600"
              iconStyle={styles.icon}
            />
            <TouchableWithoutFeedback onPress={() => this.contactFod()}>
              <View>
                <Text style={styles.contact}>
                  Une question ? Contactez-nous.
                </Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </Card>
        <View style={{ flex: 1, flexDirection: "column" }}>
          <Button
            backgroundColor="green"
            buttonStyle={{ marginTop: 10 }}
            title="Suivre mes commandes"
            onPress={() => this.props.navigation.navigate("MyOrders")}
          />

          <Button
            backgroundColor="#A7A8A8"
            buttonStyle={{ marginTop: 10 }}
            title="Retour"
            onPress={() => this.props.navigation.navigate("Menu")}
          />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    flex: 0.9,
    fontSize: 15,
    marginTop: 20
  },
  contact: {
    flex: 0.9,
    fontSize: 16,
    marginTop: 20,
    textDecorationLine: "underline"
  },
  orderValidated: {
    fontSize: 16,
    textAlign: "center",
    fontWeight: "bold"
  },
  divider: {
    backgroundColor: "grey",
    marginTop: 20,
    marginBottom: 10,
    width: "50%",
    alignSelf: "center",
    height: 2
  },
  icon: {
    left: 10,
    right: 10,
    top: 10,
    width: 60,
    height: 40
  },
  card: {
    borderRadius: 5,
    overflow: "hidden"
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

export default connect(mapStateToProps)(AcknowledgmentScreen);
