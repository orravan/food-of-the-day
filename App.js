import React from "react";
import { AsyncStorage, Platform } from "react-native";
import createStore from "./redux";
import { Provider } from "react-redux";
import NavigationStack from "./constants/NavigationStack";
import OneSignal from "./services/OneSignal";

const store = createStore();

export default class App extends React.Component {
  componentWillMount() {
    OneSignal.initOneSignal();
  }

  async componentDidMount() {
    if (Platform.OS === "ios") {
      const alreadyLaunched = await AsyncStorage.getItem("alreadyLaunched");

      if (!alreadyLaunched) {
        AsyncStorage.setItem(
          "alreadyLaunched",
          JSON.stringify({ launched: "YES" })
        );
        OneSignal.checkPermissions();
      }
    }
  }

  componentWillUnmount() {
    OneSignal.removeNotificationsListeners();
  }

  render() {
    return (
      <Provider store={store}>
        <NavigationStack />
      </Provider>
    );
  }
}
