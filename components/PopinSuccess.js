import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  Text,
  Image
} from 'react-native';

const PopinSuccess = props => {
  const {
    visible,
    text,
    ...attributes
  } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={visible}
      onRequestClose={() => {console.log('close modal')}}>
      <View style={styles.modalBackground}>
        <View style={styles.textWrapper}>
            <Image source={require('../images/success.gif')} style={{width: 250, height: 250}} />
            {null !== text && <Text style={styles.text}>{text}</Text>}
        </View>
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  textWrapper: {
    backgroundColor: '#FFFFFF',
    height: 350,
    width: 300,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  text: {
    fontSize: 17,
    color: 'green',
    textAlign: 'center',
    alignSelf: 'center'
  }
});

export default PopinSuccess;