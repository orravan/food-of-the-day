import React from "react";
import { connect } from "react-redux";
import {
  Text,
  StyleSheet,
  View,
  TouchableWithoutFeedback,
  LayoutAnimation,
  UIManager,
  Platform,
  Switch
} from "react-native";
import { Card, Button, Divider, Icon } from "react-native-elements";
import ReduxActions from "../redux/RootRedux";
import numbro from "numbro";

class CustomCard extends React.Component {
  state = {
    itemCounter: 1,
    meal: null
  };

  constructor(props) {
    super(props);

    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  componentWillMount() {
    this.setState({
      meal: this.props.meal
    });
  }

  toggleExpand(reduce) {
    LayoutAnimation.configureNext({
      duration: 300,
      create: {
        type: LayoutAnimation.Types.easeInEaseOut,
        property: LayoutAnimation.Properties.opacity
      },
      update: { type: LayoutAnimation.Types.easeInEaseOut }
    });

    this.props.expandCard(reduce ? 0 : this.props.index);
  }

  toggleExtra(index, value) {
    let meal = this.state.meal;
    meal.extra[index].selected = value;

    this.setState({
      meal: meal
    });
  }

  addItem() {
    if (this.state.meal.extra) {
      let meal = this.state.meal;
      meal.extra = meal.extra.filter(e => e.selected == true);

      this.setState({
        meal: meal
      });
    }

    let items = [];
    for (let i = 0; i < this.state.itemCounter; i++) {
      items.push(this.state.meal);
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.props.addItem(items);
    this.setState({ itemCounter: 1 });
  }

  updateCounter(value) {
    this.setState(prevState => ({
      itemCounter:
        prevState.itemCounter + value >= 0 ? prevState.itemCounter + value : 0
    }));
  }

  /**
   * Get image of the menu or return a default image if it doesn't exist
   *
   * @param {*} mealPicture
   */
  getMenuImage(mealPicture) {
    return mealPicture !== ""
      ? { uri: `${"https:" + mealPicture}` }
      : require("../images/fod.jpg");
  }

  /**
   * Depend the kind of meal, return an icon
   *
   * @param {*} typeMenu
   */
  getIconTypeMenu(typeMenu) {
    let icon = "";

    switch (typeMenu) {
      case "salade":
        icon = (
          <Icon
            size={14}
            name="leaf"
            type="font-awesome"
            style={styles.iconTypeMenu}
            color="green"
          />
        );
        break;

      case "paleo":
        icon = (
          <Icon
            size={14}
            name="fire"
            type="material-community"
            style={styles.iconTypeMenu}
            color="red"
          />
        );
        break;

      case "vegan":
        icon = (
          <Icon
            size={14}
            name="food-apple"
            type="material-community"
            style={styles.iconTypeMenu}
            color="#ff6600"
          />
        );
        break;

      default:
        break;
    }

    return icon;
  }

  render() {
    const { expandedCard, index, meal } = this.props;

    return (
      <TouchableWithoutFeedback
        style={{ flex: 1 }}
        onPress={() => (expandedCard === index ? null : this.toggleExpand())}
      >
        <Card
          image={this.getMenuImage(meal.picture)}
          containerStyle={styles.card}
          imageStyle={expandedCard === index ? styles.imageExpanded : ""}
          key={meal.sys.id}
          wrapperStyle={{ padding: 0 }}
        >
          {expandedCard !== index && (
            <View>
              <Text style={styles.title}>{meal.fields.title}</Text>
              <View style={styles.viewIconTypeMenu}>
                {this.getIconTypeMenu(meal.typeMealIcon)}
                <Text style={styles.mealDetail}>{meal.typeMealName}</Text>
              </View>
              <Text style={styles.price}>
                {numbro(meal.fields.price).format({ mantissa: 2 })} €
              </Text>
            </View>
          )}
          {expandedCard === index && (
            <View>
              <View style={styles.backButton}>
                <Button
                  icon={{
                    name: "ios-arrow-back",
                    type: "ionicon",
                    color: "#ffffff"
                  }}
                  onPress={() => this.toggleExpand(true)}
                  backgroundColor="#ff6600"
                  borderRadius={10}
                  fontSize={10}
                  title="Retour"
                />
              </View>
              <View style={styles.cardContent}>
                <View style={styles.viewIconTypeMenu}>
                  {this.getIconTypeMenu(meal.typeMealIcon)}
                  <Text style={styles.mealDetail}>{meal.typeMealName}</Text>
                </View>
                <Text style={styles.mealTitle}>{meal.fields.title}</Text>
                <Text style={styles.mealDescription}>
                  {meal.fields.description}
                </Text>
                <Text style={styles.price}>
                  {numbro(meal.fields.price).format({ mantissa: 2 })} €
                </Text>
                {this.props.user.token != "" && (
                  <Button
                    icon={{
                      name: "bowl",
                      type: "entypo"
                    }}
                    onPress={() => this.props.navigation.navigate("Allergy")}
                    backgroundColor="#ff6600"
                    buttonStyle={styles.allergy}
                    title="Une allergie ? Dites le nous !"
                  />
                )}
                {meal.extra && <Divider style={styles.divider} />}
                {meal.extra && (
                  <Text style={styles.extraTitle}>Suppléments</Text>
                )}
                {meal.extra &&
                  meal.extra.map((extra, index) => (
                    <View index={index} key={index}>
                      <Text style={styles.extraLabel}>
                        {extra.title} (+{" "}
                        {numbro(extra.price).format({ mantissa: 2 })} €)
                      </Text>
                      <Switch
                        disabled={false}
                        trackColor={{ true: "green" }}
                        style={styles.addExtra}
                        onValueChange={value => this.toggleExtra(index, value)}
                        value={
                          this.state.meal && this.state.meal.extra[index]
                            ? this.state.meal.extra[index].selected
                            : false
                        }
                      />
                      <Text style={styles.extraDescription}>
                        {extra.description}
                      </Text>
                    </View>
                  ))}
                <Divider style={styles.divider} />
                <View style={styles.buttonRow}>
                  <Icon
                    raised
                    size={16}
                    name="minus"
                    type="font-awesome"
                    color="#444"
                    onPress={() => this.updateCounter(-1)}
                  />
                  <Text style={styles.counter}>{this.state.itemCounter}</Text>
                  <Icon
                    raised
                    size={16}
                    name="plus"
                    type="font-awesome"
                    color="#444"
                    onPress={() => this.updateCounter(1)}
                  />
                </View>
                <View style={{ flex: 1, marginBottom: 20 }}>
                  <Button
                    backgroundColor="green"
                    buttonStyle={{ top: 10 }}
                    title="Ajouter au panier"
                    onPress={() => this.addItem()}
                  />
                </View>
              </View>
            </View>
          )}
        </Card>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  allergy: {
    marginBottom: 20
  },
  card: {
    borderRadius: 5,
    overflow: "hidden",
    paddingBottom: 0
  },
  buttonRow: {
    flexDirection: "row",
    alignSelf: "center"
  },
  counter: {
    fontSize: 18,
    paddingHorizontal: 15
  },
  back: {
    textDecorationLine: "underline"
  },
  cardContent: {
    height: "auto",
    marginTop: 150,
    marginBottom: 10,
    padding: 20
  },
  mealTitle: {
    fontSize: 20,
    flexDirection: "row",
    alignItems: "flex-start",
    fontWeight: "bold",
    marginBottom: 20
  },
  mealDescription: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginBottom: 20
  },
  divider: {
    backgroundColor: "grey",
    marginBottom: 20,
    width: "50%",
    alignSelf: "center",
    height: 2
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginTop: 10,
    marginLeft: 10,
    maxWidth: 220
  },
  mealDetail: {
    fontSize: 13,
    paddingLeft: 15
  },
  price: {
    position: "absolute",
    fontSize: 20,
    textAlign: "center",
    alignSelf: "flex-end",
    flex: 1,
    flexDirection: "row",
    fontWeight: "bold",
    marginTop: -25,
    marginLeft: 10,
    right: 20,
    width: 100,
    height: 50,
    backgroundColor: "white",
    borderRadius: 10,
    overflow: "hidden",
    padding: 5
  },
  extraTitle: {
    fontSize: 17,
    textAlign: "center",
    fontWeight: "bold",
    marginBottom: 20
  },
  extraLabel: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 10
  },
  extraDescription: {
    marginBottom: 20
  },
  addExtra: {
    position: "absolute",
    alignSelf: "flex-end",
    flex: 1,
    flexDirection: "row",
    marginLeft: 20,
    top: -5,
    height: 20
  },
  imageExpanded: {
    height: 200
  },
  backButton: {
    marginTop: -195,
    marginLeft: 5,
    flexDirection: "row",
    alignItems: "flex-start"
  },
  viewIconTypeMenu: {
    flex: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    alignSelf: "flex-start",
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10
  },
  iconTypeMenu: {
    width: 10,
    height: 10
  }
});

const mapStateToProps = state => {
  return {
    expandedCard: state.root.expandedCard,
    meals: state.root.meals,
    user: state.root.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    expandCard: index => dispatch(ReduxActions.expandCard(index)),
    addItem: items => dispatch(ReduxActions.addItem(items))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomCard);
