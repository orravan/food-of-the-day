import React from "react";
import { connect } from "react-redux";
import { Text, StyleSheet, View, ScrollView } from "react-native";
import { Divider, Icon } from "react-native-elements";
import _ from "lodash";
import numbro from "numbro";

class OrderDetail extends React.Component {
  /**
   * Get total price of an order
   *
   * @param {*} items
   */
  getTotalPrice(items) {
    return _.sumBy(items, item => {
      if (item.type == "sku") {
        return (item.quantity * item.amount) / 100;
      } else if (item.type == "discount") {
        return item.amount / 100;
      }
    });
  }

  render() {
    const { expanded, orders } = this.props;

    return (
      <View>
        {expanded &&
          orders.map((orderItem, index) => (
            <ScrollView key={index} style={styles.orderDetail}>
              {orderItem.metadata.lieuLivraison && (
                <View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <Icon
                      type="entypo"
                      name="location"
                      color="#ff6600"
                      iconStyle={styles.icon}
                    />
                    <Text style={styles.dateAndPlaceItems}>
                      {orderItem.metadata.lieuLivraison}
                    </Text>
                  </View>
                </View>
              )}
              <Divider style={styles.divider} />
              {orderItem.items &&
                orderItem.items.map(
                  (ci, orderItemIndex) =>
                    (ci.type == "sku" || ci.type == "discount") && (
                      <View
                        key={orderItemIndex}
                        style={{ flex: 1, flexDirection: "row" }}
                      >
                        <View style={styles.itemTitle}>
                          {ci.type == "sku" && (
                            <Text>
                              {ci.quantity} x {ci.description}
                            </Text>
                          )}
                          {ci.type == "discount" && (
                            <Text>Code promo : {ci.description}</Text>
                          )}
                        </View>
                        <View style={styles.itemPrice}>
                          <Text style={styles.itemText}>
                            {ci.type == "sku" &&
                              numbro(ci.amount / 100).format({
                                mantissa: 2
                              })}{" "}
                            {ci.type == "discount" &&
                              numbro(ci.amount / 100).format({
                                mantissa: 2
                              })}{" "}
                            €
                          </Text>
                        </View>
                      </View>
                    )
                )}
              {orderItem.items && (
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={styles.totalToPay}>Total :</Text>
                  <Text style={styles.amountToPay}>
                    {numbro(this.getTotalPrice(orderItem.items)).format({
                      mantissa: 2
                    })}{" "}
                    €
                  </Text>
                </View>
              )}
            </ScrollView>
          ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  orderDetail: {
    flex: 1,
    padding: 10
  },
  icon: {
    left: 10,
    right: 10,
    top: 10,
    width: 60,
    height: 40
  },
  dateAndPlaceItems: {
    fontSize: 15,
    fontWeight: "bold",
    width: 300,
    height: 40,
    top: 10
  },
  divider: {
    backgroundColor: "grey",
    marginBottom: 10,
    width: "50%",
    alignSelf: "center",
    height: 2
  },
  itemTitle: {
    flex: 0.8,
    flexDirection: "row",
    alignSelf: "flex-start"
  },
  itemPrice: {
    flex: 0.2,
    flexDirection: "row",
    alignSelf: "flex-end"
  },
  itemText: {
    textAlign: "center",
    justifyContent: "center",
    alignSelf: "center"
  },
  totalToPay: {
    fontSize: 15,
    fontWeight: "bold",
    flex: 0.8,
    flexDirection: "row",
    alignSelf: "flex-start"
  },
  amountToPay: {
    fontSize: 17,
    fontWeight: "bold",
    flex: 0.2,
    flexDirection: "row",
    alignSelf: "flex-end"
  }
});

const mapStateToProps = state => {
  return {
    user: state.root.user
  };
};

export default connect(mapStateToProps)(OrderDetail);
